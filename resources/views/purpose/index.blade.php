@extends('layouts/default')
{{-- Page title --}}
@section('title')
Document Types
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Document Purpose</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Document Purpose</a></li>
        <li class="active"> List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="createNewDTCode">New Document Purpose</a></span>   
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Purpose List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="pdtdatatable">
                    <thead>
                        <tr class="filters">
                            <th> #</th>
                            <th> Purpose</th>
                            <th> Generated</th>
                            <th> Last Update</th>
                            <th width="250px">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                        <tr>
                            <th> #</th>
                            <th> Purpose</th>
                            <th> Generated</th>
                            <th> Last Update</th>
                            <th> Action</th>
                        </tr>
                    </tfoot>
                </table>
                </div>
            </div>
        </div>

            <div class="modal fade" id="ajaxModelDT" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="pdtForm" name="pdtForm" class="form-horizontal">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="status" id="status">

                                <div class="form-group row">
                                    <label for="purpose" class="col-sm-3 text-right control-label col-form-label">Purpose*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="purpose" class="form-control" id="purpose" placeholder="Purpose Here">
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
        jQuery(function(){
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        }); 

            var table = jQuery('#pdtdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('purpose.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'purpose', name: 'purpose'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            jQuery('#createNewDTCode').click(function () {
                jQuery('#saveBtn').val("create-prccode");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#pdtForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Document Purpose");
                jQuery('#ajaxModelDT').modal('show');
                jQuery('.alert-danger').hide();
            });

            jQuery('body').on('click', '.editDT', function () {
                var id = jQuery(this).data('id');
                jQuery.get("{{ route('purpose.index') }}" +'/' + id +'/edit', function (data) {
                    jQuery('#modelHeading').html("Edit Document Type");
                    jQuery('#saveBtn').val("edit-purpose");
                    jQuery('#status').val('edit');
                    jQuery('#ajaxModelDT').modal('show');                   
                    jQuery('#saveBtn').html('Save changes');
                    jQuery('#id').val(data.id);
                    jQuery('#purpose').val(data.purpose);
                })
            });

            jQuery('#saveBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending..');

                jQuery.ajax({
                data: jQuery('#pdtForm').serialize(),
                url: "{{ route('purpose.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Purpose Already Exists</li>');
                    }else{
                        jQuery('#pdtForm').trigger("reset");
                        jQuery('#ajaxModelDT').modal('hide');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    $.alert({
                        theme: 'dark',
                        title: 'Missing Entry!',
                        content: 'Required input must not be blank.',
                    });
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            jQuery('body').on('click', '.deleteDTCode', function () {
                    var id = jQuery(this).data("id");
                    $.confirm({
                        theme: 'dark',
                        title: 'Please Confirm!',
                        content: 'Are you sure you want to delete this record?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('purpose.index') }}" +'/' + id +'/remove',
                                    success: function(data){
                                        table.draw();
                                    },
                                    error: function(data){
                                        console.log('Error', data);
                                    }
                                });

                            },
                            cancel: function () {
                               // $.alert('Canceled!');
                            }
                        }
                    });               
            });

    </script>

@stop