@extends('layouts/default')
{{-- Page title --}}
@section('title')
Track Document
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/jquery-confirm.min.css">


@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h3>Registered Documents</h3>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Tracking</a></li>
        <li class="active">Documents Registered</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Registered
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                    <table class="table table-bordered width100" id="trackdatatable">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tracking Number</th>
                                            <th>Type</th>
                                            <th>Title</th>
                                            <th>Purpose</th>
                                            <th>Remarks</th>
                                            <th>Office Action</th>
                                            <th>Generated</th>
                                            <th>Last Update</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>#</th>
                                            <th>Tracking Number</th>
                                            <th>Type</th>
                                            <th>Title</th>
                                            <th>Purpose</th>
                                            <th>Remarks</th>
                                            <th>Office Action</th>
                                            <th>Generated</th>
                                            <th>Last Update</th>
                                        </tr>
                                    </tfoot>
                    </table>
                </div>

            </div>
        </div>

        <div class="modal fade" id="ajaxModelDT" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>

                    <div class="card-body">
                        <form id="docForm" name="docForm" class="form-horizontal">
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="status" id="status">
                            <div class="form-group row" hidden>
                                <label for="tracking_no" class="col-sm-3 text-right control-label col-form-label">Document Number*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="tracking_no" class="form-control" id="tracking_no" style="width: 100%; height:36px;">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="type_id" class="col-sm-3 text-right control-label col-form-label">Document Type*</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" id="type_id" name="type_id" style="width: 100%; height:36px;">
                                        <option disabled>Select Document Type</option>
                                        @foreach($doctypes as $doctype)
                                            <option value="{{ $doctype->id }}">{{ $doctype->particulars }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="title" class="col-sm-3 text-right control-label col-form-label">Title/Subject*</label>
                                <div class="col-sm-9">
                                    <textarea class="form-control" rows="4" id="title" name="title" placeholder="Document Title/Subject Here. You may remove any sensitive information (names, amounts, etc.) from the title if they are not necessary in tracking document. Max Length: 250 characters"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="purpose" class="col-sm-3 text-right control-label col-form-label">Purpose*</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" id="purpose_id" name="purpose_id" style="width: 100%; height:36px;">
                                        @foreach($purposes as $purpose)
                                            <option value="{{ $purpose->id }}">{{ $purpose->particulars }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Remarks</label>
                                <div class="col-md-9">
                                <input type="text" name="remarks" class="form-control" id="remarks" placeholder="Remarks Here">
                                </div>
                            </div> 
                            <div class="form-group row">
                                <label for="payee_id" class="col-sm-3 text-right control-label col-form-label">Notify Person Concerned</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" id="payee_id" name="payee_id" style="width: 100%; height:36px;">
                                        <option value={{ NULL }}>Select Person Concerned</option>
                                        @foreach($payees as $payee)
                                            <option value="{{ $payee->id }}">{{ $payee->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>                               
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    
    <script>
       jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = jQuery('#trackdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('onroute') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },

                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'tracking_no', name: 'tracking_no'},
                        {data: 'particulars', name: 'particulars'},
                        {data: 'title', name: 'title'},    
                        {data: 'purpose', name: 'purpose'},              
                        {data: 'remarks', name: 'remarks'},
                        {data: 'office_action', name: 'office_action'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                    ],
            order: [[0, 'desc']]
            });
        });
        
    </script>
@stop

@push('scripts')

@endpush
