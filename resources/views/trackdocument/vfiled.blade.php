@extends('layouts/default')
{{-- Page title --}}
@section('title')
Track Document
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/buttons.bootstrap4.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/colReorder.bootstrap4.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/rowReorder.bootstrap4.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/scroller.bootstrap4.css') }}">

<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/jquery-confirm.min.css">


@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h3>My Documents</h3>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Tracking</a></li>
        <li class="active">Office Documents</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Logged Documents
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label for="selectFilter" class="col-md-4 text-right control-label col-form-label">Filter by:</label>
                    <div class="col-md-8">
                        <select class="select2 form-control custom-select col-md-5" id="sFilter" name="sFilter" style="width: 100%; height:36px;">
                        <option value="">Select Office Action</option>
                        <option value="Registered">Registered</option>
                        <option value="Released">Released</option>
                        <option value="Received">Received</option>
                        <option value="Filed">Filed</option>
                        </select>
                    </div>
                </div>
                    <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                        <table class="table table-striped table-bordered width100" id="filterTable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Tracking Number</th>
                                                <th>Type</th>
                                                <th>Title</th>
                                                <th>Purpose</th>
                                                <th>Remarks</th>
                                                <th>Action</th>
                                                <th>Generated</th>
                                                <th>Last Update</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>#</th>
                                                <th>Tracking Number</th>
                                                <th>Type</th>
                                                <th>Title</th>
                                                <th>Purpose</th>
                                                <th>Remarks</th>
                                                <th>Action</th>
                                                <th>Generated</th>
                                                <th>Last Update</th>
                                            </tr>
                                        </tfoot>
                        </table>
                    </div>
            </div>
        </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.buttons.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.colReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.responsive.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.rowReorder.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.colVis.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.html5.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/buttons.print.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/pdfmake.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/vfs_fonts.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.scroller.js') }}"></script>
    <script>
       jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            fetch_data();

            function fetch_data(office_action =''){
                $('#filterTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: "{{ route('vfiled') }}",
                        data: {office_action:office_action},
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'}
                    },
                    orderCellsTop: true,
                    fixedHeader: true,
                    columns: [
                            {data: 'id', name: 'id'},
                            {data: 'tracking_no', name: 'tracking_no'},
                            {data: 'particulars', name: 'particulars'},
                            {data: 'title', name: 'title'},    
                            {data: 'purpose', name: 'purpose'},              
                            {data: 'remarks', name: 'remarks'},
                            {data: 'office_action', name: 'office_action'},
                            {data: 'created_at', name: 'created_at'},
                            {data: 'updated_at', name: 'updated_at'},
                        ],
                order: [[0, 'desc']],
                buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],

                    dom: 'fBrtip',

                });
            }

            jQuery('#sFilter').change(function () {
                var sfilter_id = $('#sFilter').val();
                        console.log(sfilter_id);
                        $('#filterTable').DataTable().destroy();
                        fetch_data(sfilter_id);
            });

        });

    </script>
@stop

@push('scripts')

@endpush
