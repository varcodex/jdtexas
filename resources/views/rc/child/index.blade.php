@extends('layouts/default')
{{-- Page title --}}
@section('title')
Parent Responsibility Centers
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Parent Responsibility Centers</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Parent Responsibility Centers</a></li>
        <li class="active">PRC List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="createNewRCCode">New Responsibility Center</a></span>   
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    PRC List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="rcdatatable">
                                <thead>
                                    <tr class="filters">
                                        <th scope="col">#</th>
                                        <th scope="col">PRCName</th>
                                        <th scope="col">RCCode</th>
                                        <th scope="col">RCDescription</th>
                                        <th scope="col">Generated</th>
                                        <th scope="col">Last Update</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">PRCName</th>
                                        <th scope="col">RCCode</th>
                                        <th scope="col">RCDescription</th>
                                        <th scope="col">Generated</th>
                                        <th scope="col">Last Update</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>

            </div>
        </div>

            <div class="modal fade" id="ajaxModel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="rcForm" name="rcForm" class="form-horizontal">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="status" id="status">

                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">PRC Code</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="prc_code" name="prc_code" style="width: 100%; height:36px;">
                                            <option disabled>Select a PRC Code</option>
                                            @foreach($prcs as $prc)
                                                <option value="{{ $prc->id }}">{{ $prc->prc_code }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">RC Code*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="rc_code" class="form-control" id="rc_code" placeholder="RC Code Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">RC Description*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="rc_description" class="form-control" id="rc_description" placeholder="RC Description Here">
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
        jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        });

            var table = jQuery('#rcdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('responsibilitycenter.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'prc_code', name: 'prc_code'},
                        {data: 'rc_code', name: 'rc_code'},
                        {data: 'rc_description', name: 'rc_description'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            jQuery('#createNewRCCode').click(function () {
                jQuery('#saveBtn').val("create-prccode");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#rcForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Responsibility Center");
                jQuery('#ajaxModel').modal('show');
                jQuery('.alert-danger').hide();
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');
                $('.alert-danger').hide();
                
                $.ajax({
                data: $('#rcForm').serialize(),
                url: "{{ route('responsibilitycenter.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $.each(data.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>Responsibility Center Code Already Exists</li>');
                    }else{
                        $('#LocationForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('.alert-danger').hide();
                        $('#saveBtn').html('Save changes');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            $('body').on('click', '.editRC', function () {
                var id = $(this).data('id');

                $.get("{{ route('responsibilitycenter.index') }}" +'/' + id +'/edit', function (data) {
                    $('#modelHeading').html("Edit Responsibility Center");
                    $('#status').val('edit');
                    $('#saveBtn').val("edit-department");
                    $('#ajaxModel').modal('show');
                    $('#saveBtn').html('Save changes');
                    $('#id').val(data[0].id);
                    $('#prc_code').val(data[0].prc_id);
                    $('#rc_code').val(data[0].rc_code);
                    $('#rc_description').val(data[0].rc_description);
                })
            });


            $('body').on('click', '.deleteRC', function (){
                var id = $(this).data("id");
                $.confirm({
                        theme: 'dark',
                        title: 'Please Confirm!',
                        content: 'Are you sure you want to delete this item RCCode?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('responsibilitycenter.index') }}" +'/' + id +'/remove',
                                    success: function(data){
                                        table.draw();
                                    },
                                    error: function(data){
                                        console.log('Error', data);
                                    }
                                });

                            },
                            cancel: function () {
                                // $.alert('Canceled!');
                            }
                        }
                    });               
            });
    </script>
@stop