@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<link href="{{ asset('vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/pages/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" media="all"
      href="{{ asset('vendors/bower-jvectormap/css/jquery-jvectormap-1.2.2.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendors/animate/animate.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/pages/only_dashboard.css') }}"/>

<link href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
<meta name="_token" content="{{ csrf_token() }}">
@stop

{{-- Page content --}}
@section('content')

<section class="content-header">
    <h1>Welcome {{ strtoupper($userInfo->first_name).' '.strtoupper($userInfo->last_name) }} </h1>
    <h5> Document Tracking and Expense Aggregating System</h5>
    <ol class="breadcrumb">
    <li class=" breadcrumb-item active">
    <a href="#">
    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
        Dashboard
    </a>
    </li>
    </ol>
</section>
<section class="content indexpage">
    @include('layouts._main_content')
</section>

    <!-- Modal Form for Receiving Document Start -->
            <div class="modal fade" id="ajaxModelRD" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="rcvForm" name="rcvForm" class="form-horizontal">
                                <input type="hidden" name="id_rcv" id="id_rcv">
                                <input type="hidden" name="status_rcv" id="status_rcv">

                                <div class="form-group row">
                                    <label for="tracking_no_rcv" class="col-sm-12 control-label col-form-label">Tracking Number*</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tracking_no_rcv" class="form-control" id="tracking_no_rcv" readonly>
                                    </div> 
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveRcvBtn" value="receive">Receive the Document</button>
                                </div>
                                <br/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Modal Form for Receiving Document End -->

    <!-- Modal Form for Tagging as Terminal Start -->
    <div class="modal fade" id="ajaxModelTAG" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelTagHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="tagForm" name="tagForm" class="form-horizontal">
                                <input type="hidden" name="id_tag" id="id_tag">
                                <input type="hidden" name="status_tag" id="status_tag">

                                <div class="form-group row">
                                    <label for="tracking_no_tag" class="col-sm-12 control-label col-form-label">Tracking Number*</label>
                                    <div class="col-sm-12">
                                        <input type="text" name="tracking_no_tag" class="form-control" id="tracking_no_tag" readonly>
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveTagBtn" value="terminal">Save changes</button>
                                </div>
                                <br/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    <!-- Modal Form for Tagging as Terminal End -->


<div class="modal fade" id="editConfirmModal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alert</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>

            </div>
            <div class="modal-body">
                <p>You are already editing a row, you must save or cancel that row before edit/delete a new row</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('vendors/moment/js/moment.min.js') }}"></script>

<!-- EASY PIE CHART JS -->
<!--for calendar-->
<script src="{{ asset('vendors/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
<!--   Realtime Server Load  -->
<script src="{{ asset('vendors/flotchart/js/jquery.flot.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/flotchart/js/jquery.flot.resize.js') }}" type="text/javascript"></script>
<!--Sparkline Chart-->
<script src="{{ asset('vendors/sparklinecharts/jquery.sparkline.js') }}"></script>
<!-- Back to Top-->
<!--   maps -->
<script src="{{ asset('vendors/bower-jvectormap/js/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('vendors/bower-jvectormap/js/jquery-jvectormap-world-mill-en.js') }}"></script>
<!--  todolist-->
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-slimScroll/1.3.8/jquery.slimscroll.js"></script>--}}
<script src="{{ asset('js/pages/dashboard.js') }}" type="text/javascript"></script>
<!--//jquery-ui-->
<script src="{{ asset('js/pages/jquery-ui.min.js') }}" type="text/javascript"></script>

<script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>

<script>
       jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        });


        var table = jQuery('#officedatatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                'url':"{{ route('dashboard') }}",
                'type': 'GET',
                'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },

            orderCellsTop: true,
            fixedHeader: true,
            columns: [
                    {data: 'id', name: 'id'},
                    {data: 'barcode_id', name: 'barcode_id'},
                    {data: 'current_office', name: 'current_office'},
                    {data: 'office_action', name: 'office_action'},
                    {data: 'description', name: 'description'},  
                    {data: 'track_status', name: 'track_status'},          
                    {data: 'created_at', name: 'created_at'}, 
                ],
        order: [[0, 'desc']],
        iDisplayLength: 5
        });

        jQuery('#receiveDoc').click(function () {
            var received_id = $('#rcvtracking_no').val();
            //check if this exist in tracking table

            //check if empty
            if(! received_id==""){
                jQuery("#id").val(''); 
                jQuery('#saveRcvBtn').val("create-trackcode");
                jQuery('#status_rcv').val('receive');
                jQuery('#rcvForm').trigger("reset");
                jQuery('#modelHeading').html("Receive this Document?");
                jQuery('#ajaxModelRD').modal('show');
                jQuery('#tracking_no_rcv').val(received_id);
                jQuery('.alert-danger').hide();
            }else{
                $.alert({
                        theme: 'dark',
                        title: 'Missing Entry!',
                        content: 'Tracking number must not be blank.',
                    });
            }
        });

        jQuery('#tagDoc').click(function () {
            var tag_terminal = $('#tracking_notag').val();
             //check if empty
             if(! tag_terminal==""){
                jQuery('#saveTagBtn').val("create-trackcode");
                jQuery('#id').val('');
                jQuery('#status_tag').val('terminal');
                jQuery('#tagForm').trigger("reset");
                jQuery('#modelTagHeading').html("Are you sure you want to Tag this as Terminal?");
                jQuery('#ajaxModelTAG').modal('show');
                jQuery('#tracking_no_tag').val(tag_terminal);
                jQuery('.alert-danger').hide();
            }else{
                $.alert({
                        theme: 'dark',
                        title: 'Missing Entry!',
                        content: 'Tracking number must not be blank.',
                    });
            } 
        });

        jQuery('#saveRcvBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending may took a while to finish...');

                jQuery.ajax({
                data: jQuery('#rcvForm').serialize(),
                url: "{{ route('dashboard.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveRcvBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveRcvBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Document does not Exists</li>');

                    }else{
                        jQuery('#rcvForm').trigger("reset");
                        jQuery('#saveRcvBtn').html('Save changes');
                        jQuery('#ajaxModelRD').modal('hide');
                        jQuery('#receiveForm').trigger("reset");
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    $.alert({
                        theme: 'dark',
                        title: 'Error!',
                        content: 'Missing Data Entry.',
                    });
                    jQuery('#saveRcvBtn').html('Save Changes');
                }
                });
            });

            jQuery('#saveTagBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending may took a while to finish...');

                jQuery.ajax({
                data: jQuery('#tagForm').serialize(),
                url: "{{ route('dashboard.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveTagBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveTagBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Document does not Exists</li>');

                    }else{
                        jQuery('#tagForm').trigger("reset");
                        jQuery('#saveTagBtn').html('Save changes');
                        jQuery('#ajaxModelTAG').modal('hide');
                        jQuery('#tag_Form').trigger("reset");
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    $.alert({
                        theme: 'dark',
                        title: 'Error!',
                        content: 'Missing Data Entry.',
                    });
                    jQuery('#saveTagBtn').html('Save Changes');
                }
                });
            });

    </script>


@stop