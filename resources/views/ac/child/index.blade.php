@extends('layouts/default')
{{-- Page title --}}
@section('title')
AccountCode
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>AccountCode</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> AccountCode</a></li>
        <li class="active">AC List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <a class="btn btn-responsive btn-alignment btn-success" role="button" href="javascript:void(0)" id="createNewACCode">
                <span class="livicon" data-name="folder-add" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></span>
                New Account Code
            </a>
            
            <a class="btn btn-responsive btn-alignment btn-info" role="button" href="{{ route('printPDF')}}">
                <span class="livicon" data-name="doc-portrait" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></span>
                Export Allotment</a>         
            <div class="pull-right">

            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    AC List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="acdatatable">
                                <thead>
                                    <tr class="filters">
                                        <th scope="col">#</th>
                                        <th scope="col">Parent Account</th>
                                        <th scope="col">Allotment Class</th>
                                        <th scope="col">ACCode</th>
                                        <th scope="col">ACDescription</th>
                                        <th scope="col">Approved Budget</th>
                                        <th scope="col">Budget Adjustment</th>
                                        <th scope="col">Budget Total</th>
                                        <th scope="col">Generated</th>
                                        <th scope="col">Last Update</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Parent Account</th>
                                        <th scope="col">Allotment Class</th>
                                        <th scope="col">ACCode</th>
                                        <th scope="col">ACDescription</th>
                                        <th scope="col">Approved Budget</th>
                                        <th scope="col">Budget Adjustment</th>
                                        <th scope="col">Budget Total</th>
                                        <th scope="col">Generated</th>
                                        <th scope="col">Last Update</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>

            </div>
        </div>

            <div class="modal fade" id="ajaxModel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="acForm" name="acForm" class="form-horizontal">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="status" id="status">

                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">PAC Code</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="pac_code" name="pac_code" style="width: 100%; height:36px;">
                                            <option disabled>Select a PAC Code</option>
                                            @foreach($pacs as $pac)
                                                <option value="{{ $pac->id }}">{{ $pac->pac_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">Allotment Class</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="ac_id" name="ac_id" style="width: 100%; height:36px;">
                                            <option disabled>Select Allotment Class</option>
                                            @foreach($acs as $acc)
                                                <option value="{{ $acc->id }}">{{ $acc->uacs_particulars }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">AC Code*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="ac_code" class="form-control" id="ac_code" placeholder="AC Code Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">AC Description*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="ac_description" class="form-control" id="ac_description" placeholder="AC Description Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Approved Budget*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="approved_budget" class="form-control" id="approved_budget" placeholder="Approved Budget Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Budget Adjustment*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="budget_adjustment" class="form-control" id="budget_adjustment" placeholder="Budget Adjustment Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="lname" class="col-sm-3 text-right control-label col-form-label">Budget Total*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="budget_total" class="form-control" id="budget_total" placeholder="Budget Total Here">
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop


{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
        jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        });
            var table = jQuery('#acdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('accountcode.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'pac_description', name: 'pac_description'},
                        {data: 'uacs_particulars', name: 'uacs_particulars'},
                        {data: 'ac_code', name: 'ac_code'},
                        {data: 'ac_description', name: 'ac_description'},
                        {data: 'approved_budget', name: 'approved_budget'},
                        {data: 'budget_adjustment', name: 'budget_adjustment'},
                        {data: 'budget_total', name: 'budget_total'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            jQuery('#createNewACCode').click(function () {
                jQuery('#saveBtn').val("create-paccode");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#acForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Account Code");
                jQuery('#ajaxModel').modal('show');
                jQuery('.alert-danger').hide();
            });

            $('#saveBtn').click(function (e) {
                e.preventDefault();
                $(this).html('Sending..');
                $('.alert-danger').hide();

                $.ajax({
                data: $('#acForm').serialize(),
                url: "{{ route('accountcode.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $.each(data.errors, function(key, value){
                            $('.alert-danger').show();
                            $('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        $('.alert-danger').html('');
                        $('#saveBtn').html('Save changes');
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>Account Code Already Exists</li>');
                    }else{
                        $('#LocationForm').trigger("reset");
                        $('#ajaxModel').modal('hide');
                        $('.alert-danger').hide();
                        $('#saveBtn').html('Save changes');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            $('body').on('click', '.editAC', function () {
                var id = $(this).data('id');

                $.get("{{ route('accountcode.index') }}" +'/' + id +'/edit', function (data) {
                    $('#modelHeading').html("Edit Account Code");
                    $('#status').val('edit');
                    $('#saveBtn').val("edit-accountcode");
                    $('#ajaxModel').modal('show');
                    $('#saveBtn').html('Save changes');
                    $('#id').val(data[0].id);
                    $('#pac_code').val(data[0].pac_id);
                    $('#ac_id').val(data[0].ac_id);
                    $('#ac_code').val(data[0].ac_code);
                    $('#approved_budget').val(data[0].approved_budget);
                    $('#budget_adjustment').val(data[0].budget_adjustment);
                    $('#budget_total').val(data[0].budget_total);
                    $('#ac_description').val(data[0].ac_description);
                })
            });


            $('body').on('click', '.deleteAC', function (){
                var id = $(this).data("id");
                $.confirm({
                        theme: 'dark',
                        title: 'Please Confirm!',
                        content: 'Are you sure you want to delete this item ACCode?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('accountcode.index') }}" +'/' + id +'/remove',
                                    success: function(data){
                                        table.draw();
                                    },
                                    error: function(data){
                                        console.log('Error', data);
                                    }
                                });

                            },
                            cancel: function () {
                                // $.alert('Canceled!');
                            }
                        }
                    });               
            });
    </script>
@stop
