@extends('layouts/default')

{{-- Page title --}}
@section('title')
    Dashboard
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    {{--<link rel="stylesheet" href="{{ asset('css/pages/news.css') }}"/>--}}
    <style>
        body{
            overflow: -webkit-paged-x;
        }
    </style>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Welcome to PSU DTExAS</h1>
    <h5> Document Tracking and Expense Aggregating System</h5>
    <ol class="breadcrumb">
    <li class=" breadcrumb-item active">
    <a href="#">
    <i class="livicon" data-name="home" data-size="16" data-color="#333" data-hovercolor="#333"></i>
        Dashboard
    </a>
    </li>
    </ol>
</section>
<section class="content indexpage">
    @include('layouts._main_content')
</section>

<footer>
<div class="content indexpage">
<!-- Footer Section Start -->
    <p>Copyright &copy; DTExAS-Admin, 2020</p>
<!-- //Footer Section End -->
</div>
</footer>

<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" data-original-title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>

    @stop

{{-- page level scripts --}}
@section('footer_scripts')

@stop
