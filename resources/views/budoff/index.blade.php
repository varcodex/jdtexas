@extends('layouts/default')
{{-- Page title --}}
@section('title')
Budget
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h3>Budget Request</h3>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Budget</a></li>
        <li class="active">Request List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <a class="btn btn-responsive btn-alignment btn-success" role="button" href="{{ URL::to('budget/create') }}">
                <span class="livicon" data-name="folder-add" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></span>
                New Budget Request</a>

            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="user" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Budget Request List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="brdatatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Series</th>
                                        <th>Fund</th>
                                        <th>Payee</th>
                                        <th>Status</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Series</th>
                                        <th>Fund</th>
                                        <th>Payee</th>
                                        <th>Status</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>

            </div>
        </div>

        <div class="modal fade" id="ajaxModel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>

                    <div class="card-body">
                        <form id="brForm" name="brForm" class="form-horizontal">
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="status" id="status">
                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Document Series</label>
                                <div class="col-md-9">
                                <input type="text" name="barcode" class="form-control" id="barcode" placeholder="00-000000-0000-000" disabled>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Payee*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="payee_code" name="payee_code" style="width: 100%; height:36px;">
                                        <option disabled>Select a Payee</option>
                                        @foreach($payees as $payee)
                                            <option value="{{ $payee->id }}">{{ $payee->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Office*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="office" class="form-control" id="office" placeholder="Office Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Address*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="address" class="form-control" id="address" placeholder="Address Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Head, Requesting Office*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="sign_code_a" name="sign_code_a" style="width: 100%; height:36px;">
                                        <option disabled>Select Head of Office</option>
                                        @foreach($payees as $sign_a)
                                            <option value="{{ $sign_a->id }}">{{ $sign_a->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Head, Budget Division*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="sign_code_b" name="sign_code_b" style="width: 100%; height:36px;">
                                        <option disabled>Select Budget Chief</option>
                                        @foreach($payees as $sign_b)
                                            <option value="{{ $sign_b->id }}">{{ $sign_b->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
       jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

        });

        var table = jQuery('#brdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('budget.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },

                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'barcode', name: 'barcode'},
                        {data: 'fc_particulars', name: 'fc_particulars'},
                        {data: 'signatory_name', name: 'signatory_name'},
                        {data: 'status', name: 'status'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            jQuery('#createNewACCode').click(function () {
                jQuery('#saveBtn').val("create-prccode");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#acForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Budget Request");
                jQuery('#ajaxModel').modal('show');
                jQuery('.alert-danger').hide();
            });

            jQuery('body').on('click', '.editACCode', function () {
                var accode_id = jQuery(this).data('id');
                jQuery.get("{{ route('budget.index') }}" +'/' + accode_id +'/edit', function (data) {
                    jQuery('#modelHeading').html("Edit Budget Request (Under Construction)");
                    jQuery('#saveBtn').val("edit-user");
                    jQuery('#ajaxModel').modal('show');
                    jQuery('#saveBtn').html('Save changes');
                    jQuery('#id').val(data.id);
                    jQuery('#status').val('edit');
                    jQuery('#barcode').val(data.barcode);
                    jQuery('#payee_code').val(data.payee_id);
                    jQuery('#office').val(data.office);
                    jQuery('#address').val(data.address);
                    jQuery('#sign_code_a').val(data.signatory_A_id);
                    jQuery('#sign_code_b').val(data.signatory_B_id);
                })
            });

            jQuery('#saveBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending..');

                jQuery.ajax({
                data: jQuery('#brForm').serialize(),
                url: "{{ route('budget.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Allotment Code Already Exists</li>');
                    }else{
                        jQuery('#acForm').trigger("reset");
                        jQuery('#ajaxModel').modal('hide');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            jQuery('body').on('click', '.deleteACCode', function () {
                var ac_id = jQuery(this).data("id");
                var s = confirm("Are you sure you want to delete this item?");
                if(s){
                    $.ajax({
                        type: "GET",
                        url: "{{ route('allotmentclass.index') }}" +'/' + ac_id +'/remove',
                        success: function(data){
                            table.draw();
                        },
                        error: function(data){
                            console.log('Error', data);
                        }
                    });
                }

            });
    </script>
@stop

@push('scripts')

@endpush
