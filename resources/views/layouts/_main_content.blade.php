<section class="content indexpage">
<div class="row">
    <div class="col-lg-6 col-xl-4 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
        <!-- Trans label pie charts strats here-->
        <div class="lightbluebg no-radius">
            <div class="card-body squarebox square_boxs cardpaddng">
                <div class="row">
                    <div class="col-12 float-left nopadmar">
                        <a class="text-white stretched-link" href="{{ URL::to('documentstracking') }}">
                        <div class="row">    
                            <div class="square_box col-6 text-right">
                                    <span><h4>Register Document</h4></span>
                            </div>
                            <div class="col-6">
                                <i class="livicon  float-right" data-name="folder-add" data-l="true" data-c="#fff"
                                data-hc="#fff" data-s="70"></i>
                            </div>
                        </div>
                        </a>
                        <div class="row">
                            <div class="col-12">
                                <h4 class="text-white-50">Add New, Edit, Delete, Finalize</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xl-4 col-sm-6 col-md-6 margin_10 animated fadeInUpBig">
        <!-- Trans label pie charts strats here-->
        <div class="lightbluebg no-radius">
            <div class="card-body squarebox square_boxs cardpaddng">
                <div class="row">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-12 text-right">
                                <form method="get" action="javascript:void(0)" id="receiveForm" name="receiveForm">
                                    <span><h4>Receive Document</h4><br /></span>
                                    <div class="col-12 form-group input-group">
                                        <input type="text" class="form-control" id ="rcvtracking_no" name="rcvtracking_no" placeholder="Tracking Number Here">
                                        <div class="input-group-append">
                                                <span class="input-group-btn input-group-append">
                                                <button type="submit" class="btn-default input-group-text image_radius" id="receiveDoc"><i class="fa fa-search"></i></button>            
                                                </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>      
    <div class="col-lg-6 col-xl-4 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
        <!-- Trans label pie charts strats here-->
        <div class="lightbluebg no-radius">
            <div class="card-body squarebox square_boxs cardpaddng">
                <div class="row">
                    <div class="col-12 float-left nopadmar">
                        <div class="row">
                            <div class="square_box col-12 text-right">
                            <form method="get" action="javascript:void(0)" id="tag_Form" name="tag_Form">    
                                <span><h4>File Document</h4><br /></span>
                                <div class="col-12 form-group input-group">
                                    <input type="text" class="form-control" id ="tracking_notag" name="tracking_notag" placeholder="Tracking Number Here">
                                    <div class="input-group-append">
                                    <span class="input-group-btn input-group-append">
                                            <a class="btn btn-default input-group-text image_radius" href="javascript:void(0)" id="tagDoc"><i class="fa fa-search"></i></a>
                                            </span>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-lg-6 col-xl-12 col-12 my-3">
        <div class="card panel-border">
            <div class="card-heading bg-primary border-light">
                <h3 class="card-title">
                    <i class="livicon" data-name="eye-open" data-size="20" data-loop="true" data-c="#F89A14"
                        data-hc="#F89A14"></i>Latest Document Activities
                        @foreach($offices as $office)
                        <small>- documents registered by <b> {{ $office->rc_description }}</b> office.</small>
                        @endforeach
                </h3>
            </div>
            <div class="card-body">
                <div style="height:relative;">
                    <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                        <table class="table table-bordered width100" id="officedatatable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Series</th>
                                    <th>Current Location</th>
                                    <th>Action</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Generated</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Series</th>
                                    <th>Current Location</th>
                                    <th>Action</th>
                                    <th>Description</th>
                                    <th>Status</th>
                                    <th>Generated</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
