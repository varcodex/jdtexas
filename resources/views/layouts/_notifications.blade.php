
@foreach($notifs as $notif)
    @if($loop->iteration == 1)
<li class="dropdown notifications-menu">
    <a href="#dropdownshow" class="dropdown-toggle" data-toggle="dropdown">
        <i class="livicon" data-name="bell" data-loop="true" data-color="#e9573f"
           data-hovercolor="#e9573f" data-size="24"></i>
        <span class="label bg-warning">{{ $loop->count }}</span>
    </a>
    <ul class=" notifications dropdown-menu drop_notify" >
        <li class="dropdown-title">You have {{ $loop->count }} notifications</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class=" menu remove_hovereffect">
  @endif
                <li class="dropdown-item">
                    <i class="livicon warning" data-n="message-out" data-s="20" data-c="white"
                       data-hc="white"></i>
                    <a href="{{ url('informuser/'.$notif->id.'/') }}">{{ $notif->message }}</a>
                    <small class="float-right">
                        <span class="livicon p-2" data-n="timer" data-s="10"></span>
                         {{ $notif->created_at->diffForHumans()}}
                    </small>
                </li>
        @if($loop->iteration == $loop->count)
            </ul>
        </li>
        <li class="footer">
            <a href="#">View all</a>
        </li>
    </ul>
</li>
 @endif
@endforeach