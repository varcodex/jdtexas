<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <title>
        @section('title')
        | Welcome to DTExAS
        @show
    </title>
    <!--global css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/lib.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/custom.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon-colors.css') }}">
    <style>
      .dropdown-item:active{
            background-color: transparent !important;
        }
      .indexpage.navbar-nav >.nav-item .nav-link:hover {
          color: #01bc8c;
      }
    </style>
    <!--end of global css-->
    <!--page level css-->
    @yield('header_styles')
    <!--end of page level css-->
</head>

<body>
<!-- Header Start -->
<header>
    <!--Icon Section Start-->
    <div class="icon-section">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-8 col-md-4 mt-2">
                </div>
                <div class="col-lg-8 col-4 col-md-8 text-right mt-2">
                    <ul class="list-inline">
                        <li>
                            <a href="mailto:"><i class="livicon" data-name="mail" data-size="18" data-loop="true"
                                                 data-c="#fff"
                                                 data-hc="#fff"></i></a>
                            <label class="d-none d-md-inline-block d-lg-inline-block d-xl-inline-block"><a
                                    href="mailto:"
                                    class="text-white">jcapucao265@gmail.com</a></label>
                        </li>
                        <li>
                            <a href="tel:"><i class="livicon" data-name="phone" data-size="18" data-loop="true"
                                              data-c="#fff"
                                              data-hc="#fff"></i></a>
                            <label class="d-none d-md-inline-block d-lg-inline-block d-xl-inline-block"><a href="tel:"
                                                                                                           class="text-white">09508380963</a></label>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container indexpage">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('img/logo2.png') }}"
                                                                    alt="logo"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto  margin_right">
                    <li  class="nav-item {!! (Request::is('/') ? 'active' : '') !!}">
                        <a href="{{ route('home') }}" class="nav-link"> Home</a>
                    </li>
                    <li class="nav-item {!! (Request::is(
                    'trace') ? 'active' : '') !!}"><a href="{{ URL::to('timeline/trace') }}" class="nav-link">Track</a>
                    </li>
                    <li class="nav-item {!! (Request::is(
                    'aboutus') || Request::is('aboutus/*') ? 'active' : '') !!}"><a href="{{ URL::to('aboutus') }}" class="nav-link">
                    About Us</a>
                    </li>
                    <li class="nav-item {!! (Request::is(
                    'contact') || Request::is('contact/*') ? 'active' : '') !!}"><a href="{{ URL::to('contact') }}" class="nav-link">Contact</a>
                    </li>

                    {{--based on anyone login or not display menu items--}}
                    @if(Sentinel::guest())
                    <li class="nav-item"><a href="{{ URL::to('login') }}" class="nav-link">Login</a>
                    </li>
                    @else
                    <li class="nav-item"><a href="{{ URL::to('logout') }}" class="nav-link">Logout</a>
                    </li>
                    @endif
                </ul>
            </div>
        </nav>
        <!-- Nav bar End -->
    </div>
</header>

<!-- //Header End -->

<!-- slider / breadcrumbs section -->
@yield('top')

<!-- Content -->
@yield('content')

<!-- //Accordions Section End -->

<div class="container mt-3">
    <!-- What we are section Start -->
    <div class="row">
        <!-- What we are Start -->
        <div class="col-md-6 col-sm-6 col-lg-6 col-12 wow zoomInLeft" data-wow-duration="3s">
            <div class="text-left float-right">
                <div class="mt-2"> 
                    <h4><span class="heading_border bg-warning">What We Are</span></h4>
                </div>
            </div>
            <img src="{{ asset('images/image_12.jpg') }}" alt="image_12" class="img-fluid">
            <p>
            DTExAS is a web-based document tracking and aggregating of the expenses system. 
            Designed using Laravel 5.8, a free, open-source PHP web framework and intended 
            for the development of responsive web applications. The system allows checking 
            the status of submitted documents using mobile devices such as smartphones, laptops, or 
            desktop computers. DTExAS, however,  is not a document management system. It is
            not a document storage system. It is not meant to replace hard copies of documents,
            and it does not provide document routes. 
            </p>
            <!--<p class="text-right primary"><a href="#">Read more</a>-->
            </p>
        </div>
        <!-- //What we are End -->
        <!-- About Us Start -->
        <div class="col-md-6 col-sm-6 col-lg-6 col-12 wow zoomInRight" data-wow-duration="3s">
            <div class="text-left">
                <div class="mt-2 float-right">
                    <h4><span class="heading_border bg-success">About Us</span></h4>
                </div>
            </div>
            <img src="{{ asset('images/image_11.jpg') }}" alt="image_11" class="img-fluid">
            <p>
            DTExAS may provide State Universities and Colleges with the tools to track the paper 
            trail of documents, their lifecycles, related workflow processes, and people involved 
            within the offices. The DTExAS holds information on the originating and receiving office 
            and personnel, as well as the time elapsed between offices. Moreover, it is capable of 
            aggregating expenses from the submitted claims of stakeholders in the Budget Office.
            </p>
            <!--<p class="text-right primary my-2"><a href="#">Read more</a>-->
        </div>
        <!-- //About Us End -->
    </div>
    <!-- //What we are section End -->
</div>
    <!--</div>-->
<!-- Footer Section Start -->
<footer>
<div class=" col-12 copyright">
    <div class="container">
        <p>Copyright &copy; DTExAS-Admin, 2020</p>
    </div>
</div>
<!-- //Footer Section End -->
</footer>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" data-original-title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>



<!--global js starts-->
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}"/>
<script type="text/javascript" src="{{ asset('vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendors/wow/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontend/carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontend/lib.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/frontend/aboutus.js') }}"></script>
<!--global js end-->
<!-- begin page level js -->
@yield('footer_scripts')
<!-- end page level js -->
<script>
    $(".navbar-toggler-icon").click(function () {
        $(this).closest('.navbar').find('.collapse').toggleClass('collapse1')
    })

    $(function () {
        $('[data-toggle="tooltip"]').tooltip().css('font-size', '14px');
    })
</script>
</body>

</html>
