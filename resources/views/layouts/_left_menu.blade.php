<ul id="menu" class="page-sidebar-menu">
        <li {!! (Request::is('dashboard') ? 'class="active"' : '') !!}>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="dashboard" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Dashboard</span>
            </a>
        </li>
        <li {!! (Request::is('tracking') ? 'class="active"' : '') !!}>
            <a href="{{ URL('documentstracking') }}"> 
                <i class="livicon" data-name="folder-add" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Register Document</span>
            </a>
        </li>       

        <li {!! (Request::is('Parent') || Request::is('Child') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="gears" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Manage Documents</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('Parent') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('onroute') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Processed
                    </a>
                </li>

                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('received') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Received
                    </a>
                </li>

                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('released') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Released
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('Parent') || Request::is('Child') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="align-justify" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Data Management</span>
                <span class="fa arrow"></span>
            </a>
            
            <ul class="sub-menu">
                @if (Sentinel::inRole('budoff'))
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('parentaccountcode') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Account codes</span>
                    </a>
                </li>
                <li {!! (Request::is('Parent') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('allotmentclass') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Allotment Class</span>
                    </a>
                </li>

                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('parentresponsibilitycenter') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Campuses</span>
                    </a>
                </li>
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('fundcluster') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Fund Cluster</span>
                    </a>
                </li>
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL('mfopap') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">MFO-PAP</span>
                    </a>
                </li>
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('responsibilitycenter') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Responsibility Centers</span>
                    </a>
                </li>
                @endif
                <li {!! (Request::is('payee') ? 'class="active"' : '') !!}>
                    <a href="{{ URL('payee') }}">
                        <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white"
                        data-loop="true"></i>
                        Contact Person
                    </a>
                    </li>

                <li {!! (Request::is('purpose') ? 'class="active"' : '') !!}>
                    <a href="{{ URL('purpose') }}"> 
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Document Purpose</span>
                    </a>
                </li>              
                <li {!! (Request::is('doctype') ? 'class="active"' : '') !!}>
                    <a href="{{ URL('doctype') }}"> 
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        <span class="title">Document Types</span>
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('informuser') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('informuser') }}">
                <i class="livicon" data-name="message-out" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Send Notification</span>
            </a>
        </li>
        <li {!! (Request::is('tracking') ? 'class="active"' : '') !!}>
            <a href="{{ URL('vfiled') }}"> 
                <i class="livicon" data-name="pin-on" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">My Documents</span>
            </a>
        </li> 

@if(Sentinel::inRole('admin'))
        <li {!! (Request::is('users') || Request::is('users/create') || Request::is('user_profile') || Request::is('users/*') || Request::is('deleted_users') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="user" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Users</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('users') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('users') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Users
                    </a>
                </li>
                <li {!! (Request::is('users/create') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('users/create') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Add New User
                    </a>
                </li>
            </ul>
        </li>
        <li {!! (Request::is('groups') || Request::is('groups/create') || Request::is('groups/*') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="users" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Groups</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('groups') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('groups') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Group List
                    </a>
                </li>
                <li {!! (Request::is('groups/create') ? 'class="active" id="active"' : '') !!}>
                    <a href="{{ URL::to('groups/create') }}">
                    <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                        Add New Group
                    </a>
                </li>
            </ul>
        </li>
@elseif (Sentinel::inRole('budaid'))
            <li {!! (Request::is('budget') ? 'class="active"' : '') !!}>
                <a href="{{ URL('budget') }}">
                    <i class="livicon" data-name="piggybank" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                    data-loop="true"></i>
                    <span class="title">Budget Request</span>
                </a>
            </li>
@elseif (Sentinel::inRole('budoff'))

        <li {!! (Request::is('allotment') ? 'class="active"' : '') !!}>
            <a href="{{ URL::to('accountcode') }}">
                <i class="livicon" data-name="money" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                Budget Allotment
            </a>
        </li>

        <li {!! (Request::is('budget') ? 'class="active"' : '') !!}>
            <a href="{{ URL('budget') }}">
                <i class="livicon" data-name="piggybank" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Budget Request</span>
            </a>
        </li>

        <li {!! (Request::is('Parent') || Request::is('Child') ? 'class="active"' : '') !!}>
            <a href="#">
                <i class="livicon" data-name="presentation" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Budget Reports</span>
                <span class="fa arrow"></span>
            </a>
            <ul class="sub-menu">
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                            <a href="{{ route('printPDF')}}">
                            <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                                Budget Allotment
                            </a>
                </li>
                <li {!! (Request::is('Child') ? 'class="active" id="active"' : '') !!}>
                            <a href="{{ route('printPDF')}}">
                            <i class="livicon" data-name="angle-double-right" data-size="18" data-c="white" data-hc="white" data-loop="true"></i>
                                Budget Utilization
                            </a>
                </li>

            </ul>
        </li>

@elseif (Sentinel::inRole('accountant'))
            <li {!! (Request::is('budget') ? 'class="active"' : '') !!}>
                <a href="{{ URL('budget') }}">
                    <i class="livicon" data-name="piggybank" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                    data-loop="true"></i>
                    <span class="title">Budget Request</span>
                </a>
            </li>

@elseif (Sentinel::inRole('cashier'))
        <li {!! (Request::is('budget') ? 'class="active"' : '') !!}>
            <a href="{{ URL('budget') }}">
                <i class="livicon" data-name="piggybank" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                <span class="title">Budget Request</span>
            </a>
        </li>

        <li {!! (Request::is('payee') ? 'class="active"' : '') !!}>
            <a href="{{ URL('payee') }}">
                <i class="livicon" data-name="shield" data-size="18" data-c="#6CC66C" data-hc="#6CC66C"
                data-loop="true"></i>
                Payee
            </a>
        </li>
@endif

</ul>




