<div class="card-body">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Allotment Code</label>
        <div class="col-sm-9">
            <input type="text" name="ac_code" value="{{ old('ac_code') ?? $ac->ac_code }}" class="form-control" id="fname" placeholder="Allotment Code Here">
        </div>
    </div>

    <div class="form-group row">
        <label for="mname" class="col-sm-3 text-right control-label col-form-label">Accronym</label>
        <div class="col-sm-9">
            <input type="text" name="ac_accronym" value="{{ old('ac_accronym') ?? $ac->ac_accronym }}" class="form-control" id="mname" placeholder="Accronym Here">
        </div>
    </div>

    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Particulars</label>
        <div class="col-sm-9">
            <input type="text" name="ac_particulars" value="{{ old('ac_particulars') ?? $ac->ac_particulars }}" class="form-control" id="lname" placeholder="Allotment Particulars Here">
        </div>
    </div>
</div>

@csrf
