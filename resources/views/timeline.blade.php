@extends('layouts/defaultfront')

{{-- Page title --}}
@section('title')
Timeline
@parent
@stop

{{-- page level styles --}}
@section('header_styles')



    <link href="{{ asset('assets/zoomtimeline/zoomtimeline.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/zoomtimeline/customzoomtimeline.css') }}" rel="stylesheet">
@stop

{{-- breadcrumb --}}
@section('top')
<div class="breadcum">
        <div class="container">
            <div class="row">
            <div class="col-12 mt-1">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="16" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d" style="margin-top:-5px"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="16" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="{{ route('trace') }}">Track and Trace Documents</a>
                </li>
            </ol>
            <div class="float-right mt-1">
                <i class="livicon icon3" data-name="trace" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Track Document
            </div>
            </div>
            </div>
        </div>
</div>
@stop


{{-- Page content --}}
@section('content')
<!-- Container Section Start -->
<section class="mcon-otherdemos pat-school" style="padding-bottom: 0;
background-image: url('/images/bg1.jpg');
background-size: cover;
background-position: center center;
color: #fff;">

<div class="container">
    <div class="row " style="margin-bottom: 15px;">
            <div class="zoomtimeline mode-3dslider auto-init zoomtimeline0 skin-light circuit-the-timeline-on inited ztm-ready" data-options="{startItem: 2}" id="zoomtimeline5">
                <div class="items">
                </div>    
 
                @if(!empty($barcode))
                <div class="zoomtimeline mode-3dslider auto-init zoomtimeline0 skin-dark circuit-the-timeline-on inited ztm-ready" 
                        data-options="{startItem: 2
                        }" id="zoomtimeline5">
                            <div class="items">
                            </div>
                            <!-- the preloader START -->
                            <div class="preloader-wave">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <!-- the preloader END -->
                            <div class="yearlist-con">
                                <div class="yearlist-container">
                                    <div class="yearlist-container-inner">
                                        <div class="yearlist-line"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="details-container" style="height: 501px;"><div class="clear"></div>
                            @foreach($datas as $data)
                                <input type="radio" name="radio_btn" id="{{$data->id}}"  {{ ($data->id == $last_row->id) ? 'checked=""':'' }} >
                                <label for="{{$data->id}}" class="detail text-light ">
                                    <div class="the-year">{{ \Carbon\Carbon::parse($data->created_at)->diffForHumans() }}
                                        <figure></figure>
                                    </div>
                                    <h4 class="the-heading">Page {{ $loop->iteration }} of {{ $loop->count }}</h4>
                                    <div class="detail-image-con mt-2">
                                        <div class="detail-image--border"></div>
                                        <div class="detail-image" style="background-image: url('/images/offices/{{ $data->current_office }}.jpg')">
                                        </div>
                                            <div class="detail-image-shadow-con">
                                                <div class="detail-image-shadow-left"></div>
                                                <div class="detail-image-shadow-right"></div>
                                            </div>
                                    </div>
                                            <div class="detail-excerpt">                                 
                                                <p class="text-center text-light">                                                                 
                                                    {{$data->office_action }}
                                                    {{ \Carbon\Carbon::parse($data->created_at)->format('Y-m-d') }}
                                                    <i class="livicon icon3" data-name="clock" data-size="20" data-loop="true" data-c="#6CC66C" data-hc="#6CC66C"> </i> 
                                                    {{$data->title}}
                                                    @if(!empty($data->remarks))
                                                        <br/>FYI:  
                                                        <i class="livicon icon3" data-name="question" data-size="20" data-loop="true" data-c="#ff6666" data-hc="#ff6666"> </i> 
                                                         {{$data->remarks}}.
                                                    @endif
                                                    <h6 class="text-center text-success"></h6>
                                                </p>
                                            </div>  
                                        <div class="clear"></div>
                                </label>
                            @endforeach
            
                            </div>
                </div>
                @endif   
                
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 mt-3">
                                    <div class="float-right mx-auto">
                                    <form action="{{ route('trace') }}" autocomplete="on" method="get" class="my-3">
                                        <div class="input-group">
                                            <input class="form-control" id="id" name="id" required type="text" value="{{ $barcode }}" placeholder="Input Document Number." aria-label="Document Number" aria-describedby="basic-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-success" type="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                        </div>
                    </div>
                </div>    

        </div>
    </div>    
</div>
</section>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
@stop
