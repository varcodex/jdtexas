<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="title" content="Document Tracking and Expenses Aggregating System">
    <meta name="description" content="PSU DTExAS">
    <meta name="author" content="Joni Neil B. Capucao, DIT">
    <meta name="keywords" content="Document Tracking, Aggregation of Expenses, SUC's, Partido State University">
    <title>
        DTExAS Main
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('images/favicon.png') }}">
    {{--CSRF Token--}}
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- global css -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
     <!-- end of global level css -->
    <!-- page level css -->
    <link rel="stylesheet" href="{{ asset('vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/pages/only_dashboard.css') }}"/>
    <link rel="stylesheet" href="{{ asset('css/pages/jbc_custom.css') }}"/>
    <!-- end of page level css -->

 </head>

<body>  
<div id="choose" class="card-deck col-sm-12 col-sm-offset-3 col-md-8 col-md-offset-4 col-lg-4 col-lg-offset-4 col-xl-4 mx-auto">
    <div class="card-body bg-light border-dark padding-15">
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="black_bg no-radius">
                    <div class="card-body squarebox square_boxs cardpaddng">
                        <div class="row">
                            <div class="col-12 float-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-12 pull-left text-center text-white">  
                                        <h4><img src="{{ asset('img/logo.png') }}" alt="logo">
                                        </h4>
                                        <h6>Partido State University Document Tracking<br/> & Expense Aggregating System</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
        <hr />
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInRightBig">
                <!-- Trans label pie charts strats here-->
                <div class="palebluecolorbg no-radius">
                    <div class="card-body squarebox square_boxs cardpaddng">
                        <div class="row">
                            <div class="col-12 float-left nopadmar">
                            <a class="text-white stretched-link" href="{{ route('login') }}">
                                <div class="row">
                                    <div class="square_box col-8 pull-left">     
                                        <h4>
                                            <br />
                                            Login for Registered Users
                                            
                                        </h4>
                                    </div>
                                    <div class="col-4">
                                        <i class="livicon float-right" data-name="users" data-l="true" data-c="#fff"
                                            data-hc="#fff" data-s="70"></i>
                                    </div>
                                </div>
                                </a>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInUpBig">
                <!-- Trans label pie charts strats here-->
                <div class="lightbluebg no-radius">
                    <div class="card-body squarebox square_boxs cardpaddng">
                        <div class="row">
                            <div class="col-12 float-left nopadmar">
                                <a class="text-white stretched-link" href="{{ route('trace') }}">
                                <div class="row">
                                    <div class="square_box col-8 pull-left">
                                    <br/>
                                    <h4>
                                                Track and Trace Documents</h4>
                                   </div>
                                    <div class="col-4">
                                        <i class="livicon  float-right" data-name="trace" data-l="true" data-c="#fff"
                                        data-hc="#fff" data-s="70"></i>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInLeftBig">
                <!-- Trans label pie charts strats here-->
                <div class="redbg no-radius">
                    <div class="card-body squarebox square_boxs cardpaddng">
                        <div class="row">
                            <div class="col-12 float-left nopadmar">
                            <a class="text-white stretched-link" href="{{ route('aboutus') }}">
                                <div class="row">
                                    <div class="square_box col-8 float-left">
                                    <br/>
                                    <h4>
                                                About Us</h4>                                       
                                    </div>
                                    <div class="col-4">
                                        <i class="livicon float-right" data-name="info" data-l="true" data-c="#fff"
                                        data-hc="#fff" data-s="70"></i>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInDownBig">
                <!-- Trans label pie charts strats here-->
                <div class="goldbg no-radius">
                    <div class="card-body squarebox square_boxs cardpaddng">
                        <div class="row">
                            <div class="col-12 float-left nopadmar">
                            <a class="text-white stretched-link" href="{{ route('contact') }}">
                                <div class="row">
                                    <div class="square_box col-8 pull-left">
                                    <br/>
                                    <h4>
                                            Contact Us</h4> 
                                    </div>
                                    <div class="col-4">
                                        <i class="livicon float-right" data-name="cellphone" data-l="true" data-c="#fff"
                                            data-hc="#fff" data-s="70"></i>
                                    </div>
                                </div>
                            </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 margin_10 animated fadeInUpBig">
            <h6 class="text-center">Copyright &copy; PSU DTExAS-Admin, 2020</h6>
        </div>
    </div>
</div>


<!-- global js -->

<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>


<!-- end of global js -->

</body>
</html>