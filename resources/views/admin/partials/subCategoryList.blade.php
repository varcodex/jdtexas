@foreach($subcategories as $subcategory)
<tr data-id="{{$subcategory->ac_id}}" data-parent="{{$dataParent}}" data-level="{{$dataLevel + 1}}"> 
    <td ></td>
    <td class="desc" data-column="name">{{$subcategory->ac_description}}</td>
    <td class="amount" data-column="approved_budget">{{number_format($subcategory->approved_budget,2)}}</td>
    <td class="amount" data-column="budget_adjustment">{{number_format($subcategory->budget_adjustment,2)}}</td>
    <td class="amount" data-column="total">{{number_format($subcategory->approved_budget + $subcategory->budget_adjustment,2)}}</td>
    <td class="amount" data-column="budget_total">{{number_format($subcategory->budget_total,2)}}</td>
</tr>
    @if(count($subcategory->subcategory))
        @include('admin.partials.subCategoryList',['subcategories'=>$subcategory->subcategory,'dataParent'=>$subcategory->ac_id,'dataLevel'=> $dataLevel ])
    @endif
@endforeach

<tr>
    <td colspan="2" class="grand total">Sub Total</td>
    @foreach($subTotals as $subtotal)
        @if($subtotal->ac_id === $subcategory->ac_id)
            <td class="grand total" data-column="approved_budgetT">{{number_format($subtotal->approved_budgetT,2)}}</td>
            <td class="grand total" data-column="budget_adjustmentT">{{number_format($subtotal->budget_adjustmentT,2)}}</td>
            <td class="grand total" data-column="totalT">{{number_format($subtotal->approved_budgetT + $subtotal->budget_adjustmentT,2)}}</td>
            <td class="grand total" data-column="budget_totalT">{{number_format($subtotal->budget_totalT,2)}}</td>
        @endif
    @endforeach
</tr>