@extends('layouts/default')
{{-- Page title --}}
@section('title')
Payee
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Contact Person</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#">Person In-charge</a></li>
        <li class="active">Personnels</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="createNewPayee">New Personnel</a></span>
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="users" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Personnel List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="signdatatable">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th width="180px">Name</th>
                                        <th width="180px">Designation</th>
                                        <th width="180px">Address</th>
                                        <th width="100px">Email Address</th>
                                        <th>Responsibility Center</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="100px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Designation</th>
                                        <th>Address</th>
                                        <th>Email Address</th>
                                        <th>Responsibility Center</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>

            </div>
        </div>

        <div class="modal fade" id="ajaxModel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>


                    <div class="card-body">
                        <form id="payeeForm" name="payeeForm" class="form-horizontal">
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="status" id="status">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Name*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="sign_name" class="form-control" id="sign_name" placeholder="Name Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">designation*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="sign_designation" class="form-control" id="sign_designation" placeholder="designation Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Address*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="address" class="form-control" id="address" placeholder="Address Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Email Address*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="email_address" class="form-control" id="email_address" placeholder="Email Address Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Responsibility Center</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="rc_code" name="rc_code" style="width: 100%; height:36px;">
                                        <option disabled>Select a RC Code</option>
                                        @foreach($rcs as $rc)
                                            <option value="{{ $rc->id }}">{{ $rc->rc_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">is Signatory?</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="is_signatory" name="is_signatory" style="width: 100%; height:36px;">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
        jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var table = jQuery('#signdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('payee.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'sign_name', name: 'sign_name'},
                        {data: 'sign_designation', name: 'sign_designation'},
                        {data: 'address', name: 'address'},
                        {data: 'email_address', name: 'email_address'},
                        {data: 'rc_description', name: 'rc_description'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });

            jQuery('#createNewPayee').click(function () {
                jQuery('#saveBtn').val("create-payee");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#payeeForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Payee");
                jQuery('#ajaxModel').modal('show');
                jQuery('.alert-danger').hide();
            });

            jQuery('body').on('click', '.editPayee', function () {
                var payee_id = jQuery(this).data('id');
                jQuery.get("{{ route('payee.index') }}" +'/' + payee_id +'/edit', function (data) {
                    jQuery('#modelHeading').html("Edit Payee");
                    jQuery('#saveBtn').val("edit-user");
                    jQuery('#ajaxModel').modal('show');
                    jQuery('#saveBtn').html('Save changes');
                    jQuery('#id').val(data[0].id);
                    jQuery('#status').val('edit');
                    jQuery('#sign_name').val(data[0].sign_name);
                    jQuery('#sign_designation').val(data[0].sign_designation);
                    jQuery('#address').val(data[0].address);
                    jQuery('#email_address').val(data[0].email_address);
                    jQuery('#rc_code').val(data[0].rc_id);
                    jQuery('#is_signatory').val(data[0].is_signatory);
                })
            });

            jQuery('#saveBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending..');

                jQuery.ajax({
                data: jQuery('#payeeForm').serialize(),
                url: "{{ route('payee.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Payee Already Exists</li>');
                    }else{
                        jQuery('#payeeForm').trigger("reset");
                        jQuery('#ajaxModel').modal('hide');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            jQuery('body').on('click', '.deletePayee', function () {
                var payee_id = jQuery(this).data("id");
                var s = confirm("Are you sure you want to delete this item Payee?");
                if(s){
                    $.ajax({
                        type: "GET",
                        url: "{{ route('payee.index') }}" +'/' + payee_id +'/remove',
                        success: function(data){
                            table.draw();
                        },
                        error: function(data){
                            console.log('Error', data);
                        }
                    });
                }

            });


        });
    </script>
@stop

@push('scripts')

@endpush
