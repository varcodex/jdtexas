@extends('layouts/default')
{{-- Page title --}}
@section('title')
Funding Sources
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Funding Sources</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Funding Sources</a></li>
        <li class="active">Funding Sources List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="createNewFCCode">New Funding Sources</a></span>
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                Fund Cluster List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="fcdatatable">
                                <thead>
                                    <tr class="filters">
                                        <th>#</th>
                                        <th>FundCode</th>
                                        <th>FundDescription</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>FundCode</th>
                                        <th>FundDescription</th>
                                        <th>Generated</th>
                                        <th>Last Update</th>
                                        <th width="280px">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>

            </div>
        </div>

        <div class="modal fade" id="ajaxModel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                    <div class="modal-header">
                        <h4 class="modal-title" id="modelHeading"></h4>
                    </div>


                    <div class="card-body">
                        <form id="fcForm" name="fcForm" class="form-horizontal">
                            <input type="hidden" name="id" id="id">
                            <input type="hidden" name="status" id="status">
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Fund Cluster Code*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="fc_code" class="form-control" id="fc_code" placeholder="Fund Cluster Code Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">FC Particulars*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="fc_particulars" class="form-control" id="fc_particulars" placeholder="FC Particulars Here">
                                </div>
                            </div>

                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
<script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
<script>
    jQuery(function() {
        jQuery.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
        });
    });
    
        var table = jQuery('#fcdatatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                'url':"{{ route('fundcluster.index') }}",
                'type': 'GET',
                'headers': {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            },

            orderCellsTop: true,
            fixedHeader: true,
            columns: [
                    {data: 'id', name: 'id'},
                    {data: 'fc_code', name: 'fc_code'},
                    {data: 'fc_particulars', name: 'fc_particulars'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ],
        order: [[0, 'desc']]
        });

        jQuery('#createNewFCCode').click(function () {
            jQuery('#saveBtn').val("create-fccode");
            jQuery('#id').val('');
            jQuery('#status').val('create');
            jQuery('#fcForm').trigger("reset");
            jQuery('#modelHeading').html("Create New Fund Cluster");
            jQuery('#ajaxModel').modal('show');
            jQuery('.alert-danger').hide();
        });

        jQuery('body').on('click', '.editFCCode', function () {
            var fccode_id = jQuery(this).data('id');
            jQuery.get("{{ route('fundcluster.index') }}" +'/' + fccode_id +'/edit', function (data) {
                jQuery('#modelHeading').html("Edit Fund Cluster");
                jQuery('#saveBtn').val("edit-user");
                jQuery('#ajaxModel').modal('show');
                jQuery('#saveBtn').html('Save changes');
                jQuery('#id').val(data.id);
                jQuery('#status').val('edit');
                jQuery('#fc_code').val(data.fc_code);
                jQuery('#fc_particulars').val(data.fc_particulars);
            })
        });

        jQuery('#saveBtn').click(function (e) {
            e.preventDefault();
            jQuery(this).html('Sending..');

            jQuery.ajax({
            data: jQuery('#fcForm').serialize(),
            url: "{{ route('fundcluster.store') }}",
            type: "POST",
            dataType: 'json',
            success: function (data) {
                if(data.errors){
                      jQuery('.alert-danger').html('');
                    jQuery('#saveBtn').html('Save changes');
                      jQuery.each(data.errors, function(key, value){
                          jQuery('.alert-danger').show();
                          jQuery('.alert-danger').append('<li>'+value+'</li>');
                      });

                  }else if(data.code == "1"){
                    jQuery('.alert-danger').html('');
                    jQuery('#saveBtn').html('Save changes');
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<li>FC Code Already Exists</li>');
                }else{
                    jQuery('#fcForm').trigger("reset");
                    jQuery('#ajaxModel').modal('hide');
                    table.draw();
                  }

            },
            error: function (data) {
                console.log('Error:', data);
                jQuery('#saveBtn').html('Save Changes');
            }
            });
        });

        jQuery('body').on('click', '.deleteFCCode', function () {
            var fccode_id = jQuery(this).data("id");
            $.confirm({
                        theme: 'dark',
                        title: 'Please Confirm!',
                        content: 'Are you sure you want to delete this item?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('fundcluster.index') }}" +'/' + fccode_id +'/remove',
                                    success: function(data){
                                        table.draw();
                                    },
                                    error: function(data){
                                        console.log('Error', data);
                                    }
                                });

                            },
                            cancel: function () {
                                // $.alert('Canceled!');
                            }
                        }
                    });               
            });

</script>
@stop

@push('scripts')

@endpush
