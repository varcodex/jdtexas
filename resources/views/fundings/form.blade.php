<div class="card-body">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">Funding Cluster Code</label>
        <div class="col-sm-9">
            <input type="text" name="fc_code" value="{{ old('fc_code') ?? $prc->fc_code }}" class="form-control" id="fname" placeholder="Fund Cluster Code Here">
        </div>
    </div>

    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Particulars</label>
        <div class="col-sm-9">
            <input type="text" name="fc_particulars" value="{{ old('fc_particulars') ?? $prc->fc_particulars }}" class="form-control" id="lname" placeholder="Fund Cluster Particulars Here">
        </div>
    </div>
</div>

@csrf
