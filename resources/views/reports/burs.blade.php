<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
<table class="table table-bordered" style="width: 948px;">
   <tbody>
    <tr>
      <td rowspan="3" width="101" ><img src="PSUlogo.png" alt="logo" height="90" width="100"></td>
      <td rowspan="3" colspan="4" width="400"  height="30" align="center"><h6>BUDGET UTILIZATION REQUEST AND STATUS</h6><br/>PARTIDO STATE UNIVERSITY <br/>San Juan Bautista St., Goa, Camarines Sur</td>
      <td width="120" height="30">Serial No:</td>
      <td colspan="2" width="240" height="30">{{$ch->barcode}}</td>
    </tr>
    <tr>
      <td>Date:</td>
      <td colspan="2">January 1, 2020</td>
    </tr>
    <tr>
      <td>Fund Cluster:</td>
      <td colspan="2">{{$ch->fundClusters}}</td>
    </tr>

    <tr>
      <td colspan="1" width="101">Payee</td>
      <td colspan="7">{{$ch->payeesName}}</td>
    </tr>
    <tr>
      <td colspan="1" width="101">Office</td>
      <td colspan="7">{{$ch->office}}</td>
    </tr>
    <tr>
      <td colspan="1" width="101">Address</td>
      <td colspan="7">{{$ch->address}}</td>
    </tr>
    <tr>
      <td colspan="1" width="101" align="center">Responsibility Center</td>
      <td colspan="3" width="401" align="center">Particulars</td>
      <td colspan="1" width="101" align="center">MFO/PAP</td>
      <td colspan="1" width="101" align="center">UACS Object Code Expenditures</td>
      <td colspan="2" align="center">Amount</td>
    </tr>
    @foreach ($chdetails as $chdetail)
    <tr style="border-bottom-style:none">
    <td colspan="1" width="101" align="center">{{$chdetail->rcCode}}</td>
      <td colspan="3" width="401" align="center">{{$chdetail->particulars}}</td>
      <td colspan="1" width="101" align="center">{{$chdetail->mfoCode}}</td>
      <td colspan="1" width="101" align="center">{{$chdetail->acCode}}</td>
      <td colspan="2" align="center">{{$chdetail->amount}}</td>
    </tr>
    @endforeach
    <tr style="border-top-style:none">
      <td colspan="1" width="101" align="center"></td>
      <td colspan="3" width="301" align="center"></td>
      <td colspan="1" width="101" align="center"></td>
      <td colspan="1" width="101" align="center">Total</td>
      <td colspan="2" align="center" >{{$total}}</td>
    </tr>
    <tr>
      <td colspan="4" width="50%" align="center">
      	<h6>A. Certified: Charges to appropriation/budget necessary, lawful and under my direct supervision; and supporting documents valid, proper and legal
      	<br/><br/>
      	<pre>
      	Signature: ____________________________________________ <br/>
      	Printed Name: {{$signA->payeesName}} <br/>
      	Position: _____________________________________________ <br/>
      	Date: _________________________________________________ <br/>
      </pre></h6>
      </td>
      <td colspan="4" width="50%" align="center">
      	<h6>B. Certified: Budget available and utilized for the purpose / adjustment necessary as indicated above.
      <br/><br/><br/>
      <pre>
      	Signature: ____________________________________________ <br/>
      	Printed Name: {{$signA->payeesName}} <br/>
      	Position: _____________________________________________ <br/>
      	Date: _________________________________________________ <br/>
      </pre></h6>
      </td>
    </tr>
   <tr>
      <td colspan="1" width="101" align="left">C.</td>
      <td colspan="7" align="center">STATUS OF UTILIZATION</td>
    </tr>
   <tr>
      <td colspan="3" align="center">Reference</td>
      <td colspan="5" align="center">Amount</td>
    </tr>
   <tr>
      <td rowspan="3" align="center">Date</td>
      <td rowspan="3" align="center">Particulars <br><br>{{$ch->particulars}}</td>
      <td rowspan="3" align="center">BURS/JEV/RCI RADAI/RTRAI No.</td>
      <td align="center">Utilization</td>
      <td align="center">Payable</td>
      <td align="center">Payment</td>
      <td colspan="2" align="center">Balance</td>
    </tr>
    <tr>
    	<td rowspan="2" align="center">(a)</td>
    	<td rowspan="2" align="center">(b)</td>
    	<td rowspan="2" align="center">(c)</td>
    	<td align="center">Not Yet Due</td>
    	<td align="center">Due and Demandable</td>
    </tr>
    <tr>
    	<td align="center">(a-b)</td>
    	<td align="center">(b-c)</td>
    </tr>
    <tr>
    	<td></td>
     	<td></td>
	   	<td></td>
       	<td></td>
       	<td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
  </tbody>
</table>
</div>
</body>
</html>
