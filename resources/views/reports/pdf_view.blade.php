<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>PSU Budget Allotments</title>
<link rel="stylesheet" href="css/dompdfs.css" media="all" />
</head>
<body>
    <header class="clearfix">
        <div id="logo">
          <img src="img/NewPSUlogo.jpg" style="height:70px; width:70px;">
        </div>
        <div id="company" class="clearfix">
            Partido State University<br>
            Goa, Camarines Sur<br>
        </div>

        <div id="project">
        <div><B>Statement of Approved Budget and Balances</B></div>
        <div>School Year 2019-2020</div>
        <div>as of {{Now()}}</div>
        </div>
        
    </header>
  <main>
    <table width="720px">
        <thead>
          <tr>
            <th ></th>
            <th class="desc">ACCOUNT</th>
            <th class="service">BUDGET</th>
            <th class="service">ADJUSTMENT</th>
            <th class="service">TOTAL</th>
            <th class="service">BALANCE</th>
          </tr>
        </thead>
        <tbody>
            @foreach($parentCategories as $category)
                <tr data-id="{{$category->ac_id}}" data-parent="0" data-level="1">
                    <td colspan="6" class="desc" data-column="ac_description">{{$category->ac_description}}</td>
                </tr>
                @if(count($category->subcategory))
                    @include('admin.partials.subCategoryList',['subcategories' => $category->subcategory,'dataParent'=>$category->ac_id,'dataLevel'=> 1])
                @endif
            @endforeach
            @foreach($grandTotals as $grandtotal)
                <tr> 
                  <td colspan="2" class="grand total">GRAND TOTAL</td>
                  <td class="grand total" data-column="approved_budgetGT">{{number_format($grandtotal->approved_budgetGT,2)}}</td>
                  <td class="grand total" data-column="budget_adjustmentGT">{{number_format($grandtotal->budget_adjustmentGT,2)}}</td>
                  <td class="grand total" data-column="totalGT">{{number_format($grandtotal->approved_budgetGT + $grandtotal->budget_adjustmentGT,2)}}</td>
                  <td class="grand total" data-column="budget_totalGT">{{number_format($grandtotal->budget_totalGT,2)}}</td>
                </tr>
            @endforeach
                <tr><td colspan="6" class="grand total"></td>></tr>
        </tbody>
      </table>
    </main>
  <footer>
      Copyright@2020 DTExAS-Partido State University, Goa, Camarines Sur.
    </footer>
</body>
</html>