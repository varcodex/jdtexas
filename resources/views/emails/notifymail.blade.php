<p> Greetings {{ $username}}, </p>
<p> Your document has been received and being processed.</p> 
<p> Please click the link provided to view the whereabouts of the document.</p>
@component('mail::panel')
<p> {{ $tracking_no }}</p>
@endcomponent
<br/>
<p>For your comments and suggestions, you may contact us through our website at http://jcapucao265.com.</p><br/> 
Thank you for your continous support to the University, <br/>
<p>DTExAS-Administrator</p>
