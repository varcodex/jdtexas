@component('mail::message')
# Introduction

The body of your message.

@component('mail::button', ['url' => 'jcapucao265.com/timeline'])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
