@extends('layouts/defaultfront')

{{-- Page title --}}
@section('title')
Contact
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/contact.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-lg-block d-sm-block d-md-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Contact</a>
                </li>
            </ol>
            <div class="float-right mt-1">
                <i class="livicon icon3" data-name="cellphone" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Contact
            </div>
        </div>
    </div>
        </div>
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <div class="row">
            <!-- Contact form Section Start -->
            <div class="col-md-6 col-lg-6 col-12 my-3">
                <div class="card card-body">
                <h3>Contact Us</h3>
                <!-- Notifications -->
                <div id="notific">
                    @include('notifications')
                </div>
                <form class="contact" id="contact" action="{{ url('/contact') }}" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <input type="text" name="name" value="{{ old('name') }}" class="form-control input-lg" placeholder="Your name">
                        <div>{{ $errors->first('name')}}</div>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" value="{{ old('email') }}" class="form-control input-lg" placeholder="Your email address">
                        <div>{{ $errors->first('email')}}</div>
                    </div>
                    <div class="form-group">
                        <textarea name="content" value="{{ old('content') }}" class="form-control input-lg no-resize resize_vertical" rows="6" placeholder="Your comment"></textarea>
                        <div>{{ $errors->first('content')}}</div>
                    </div>
                    <div class="input-group">
                        <button class="btn btn-primary mr-1" type="submit">Send Message</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                    </div>
                </form>
                </div>
            </div>
            <!-- //Conatc Form Section End -->
            <!-- Address Section Start -->
            <div class="col-md-6 col-sm-6" id="address_margt">
                <div class="media media-top media-right">
                    <a href="#">
                    <div class="box-icon">
                    <i class="livicon" data-name="home" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                    </div>
                    </a>
                    <div class="media-body ml-3">
                        <h4 class="media-heading">Address:</h4>
                        <div class="danger">Partido State University</div>
                        <address>
                        San Juan Bautista,
                        <br> Goa, Camarines Sur
                        <br> Philippines, 4421
                        </address>
                    </div>
                </div>
                <div class="media media-top">
                    <a href="#">
                    <div class="box-icon">
                    <i class="livicon" data-name="phone" data-size="22" data-loop="true" data-c="#fff" data-hc="#fff"></i>
                    </div>
                    </a>
                    <div class="media-body ml-3">
                        <h4 class="media-heading">Mobile No:</h4> 
                        Smart: 0950 838 0963
                        <br /> Globe: 0917 142 9721
                    </div>
                </div>
            </div>
            <!-- //Address Section End -->
        </div>
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
@stop
