@extends('layouts/defaultfront')

{{-- Page title --}}
@section('title')
About us
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/aboutus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/devicon/devicon-colors.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">About Us</a>
                </li>
            </ol>
            <div class="float-right mt-1">
                <i class="livicon icon3" data-name="users" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> About Us
            </div>
        </div>
    </div>

        </div>
    </div>
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!-- Slider Section Start -->
        <div class="row my-3">
            <!-- Left Heading Section Start -->
            <div class="col-md-12 col-sm-12 col-lg-12 col-12">
                <div class="row">
                <!-- Overview -->    
                <div class="col-md-12 col-sm-12 col-12 col-lg-4 wow zoomIn" data-wow-duration="3s">
                        <div class="box">
                            <div class="box-icon">
                                <i class="livicon icon1" data-name="medal" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                            </div>
                            <div class="info">
                                <h3 class="primary text-center mt-3">DTExAS</h3>
                                <div class="text-center default_color">
                                <h4>We're passionate about helping public office personnel boost their productivity.</h4>
                                <p>DTExAS provides State Universities and Colleges with the tools to track the paper trail of documents, their lifecycles, related workflow processes, and people involved within the offices and employees. The DTExAS includes information on the originating and receiving office and personnel, as well as the time elapsed between offices/units/colleges. Moreover, it is capable of aggregating expenses from the submitted claims of stakeholders in the office of Budget and Finance.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Responsive Section Start -->
                    <div class="col-md-12 col-sm-12 col-12 col-lg-4 wow zoomIn" data-wow-duration="3s">
                        <div class="box">
                            <div class="box-icon box-icon2">
                                <i class="livicon icon1" data-name="heart" data-size="55" data-loop="true" data-c="#ef6f6c" data-hc="#ef6f6c"></i>
                            </div>
                            <div class="info">
                                <h3 class="danger text-center mt-3">Our Values</h3>
                                <div class="text-center">
                                <h4>We're enthusiastic about business process management technologies.</h4>
                                    <img src="{{ asset('img/DTExAS_core.png') }}" width ="70%">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Responsive Section End -->
                    <!-- Easy to Use Section Start -->
                    <div class="col-md-12 col-sm-12 col-12 col-lg-4 wow zoomIn" data-wow-duration="3s">
                        <div class="box">
                            <div class="box-icon box-icon1">
                                <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                            </div>
                            <div class="info">
                                <h3 class="success text-center mt-3">Easy to Use</h3>
                                    <p> DTExAS may provide State Universities and Colleges with features which tracks different attributes and locations of documents. 
                                    Through document tracking you can control who can view a document, check to see who has accessed the document, who has made changes to it, 
                                    and who the document has been sent to. Document tracking also allows management to easily view the progress that has been made on documents. 
                                    Document tracking allows you to quickly see the full list of people who accessed the documents. </p>
                                        <div class="text-right primary"><a href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Easy to use Section End -->
                </div>
            <!-- //Left Heading Section End -->
            </div>
        </div>
        <!-- //Slider Section End -->
        <!-- Services Section Start -->

        <!-- // Services Section End -->
    </div>

@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/frontend/aboutus.js') }}"></script>
    <!--page level js ends-->
@stop
