@extends('layouts/default')

{{-- Page title --}}
@section('title')
User Notification
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/contact.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-lg-block d-sm-block d-md-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Notify</a>
                </li>
            </ol>
            <div class="float-right mt-1">
                <i class="livicon icon3" data-name="cellphone" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Contact
            </div>
        </div>
    </div>
        </div>
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <section class="content indexpage">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-8 col-12 my-3">
                <div class="card panel-border">
                    <div class="card-heading">
                        <h3 class="card-title">
                            <i class="livicon" data-name="message-out" data-size="20" data-loop="true" data-c="#F89A14"
                            data-hc="#F89A14"></i>
                            Send Notification
                            <small>- to various offices</small>
                        </h3>
                    </div>
                    <div class="card-body">
                        <form class="contact" id="informuser" action="{{ url('/informuser') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        
                            <div class="form-group row">
                                <label for="to_rc_id" class="col-sm-12 control-label col-form-label">To Office*</label>
                                <div class="col-sm-12">
                                    <select class="select2 form-control custom-select" id="to_rc_id" name="to_rc_id" style="width: 100%; height:36px;">
                                        <option>Please select the office to notify</option>
                                        @foreach($rcs as $rc)
                                            <option value="{{ $rc->id }}">{{ $rc->rc_description }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="smessage" class="col-sm-12 control-label col-form-label">Short Message (Max 40 Characters)*</label>
                                <div class="col-sm-12">
                                    <input type="text" name="smessage" id="smessage" class="col-sm-12 form-control" maxlength="40" placeholder="">
                                </div>
                            </div>
                            <div class="input-group">
                                <button class="btn btn-primary mr-1" type="submit">Send Message</button>
                                <button class="btn btn-danger" type="reset">Reset</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6 col-xl-4 col-12 my-3">
                <div class="card panel-border">
                        <div class="card-heading  panel-lightgreen border-light">
                            <h4 class="card-title">
                                <i class="livicon" data-name="message-out" data-size="16" data-loop="true" data-c="#fff"
                                data-hc="#fff"></i> Recent Notifications
                            </h4>
                        </div>
                        <div class="card-body nopadmar">
                            @foreach($allnotifs as $allnotif )
                            <div class="media">
                                <div class="media-body panel-default ml-3">
                                    @if(! empty($allnotif->is_read))
                                    <h5 class="media-heading"></h5>
                                    <p>{{ $allnotif->message }} <span
                                            class="user_create_date float-right">{{ $allnotif->created_at->diffForHumans() }} </span></p>
                                    @endif
                                </div>
                            </div>
                            @endforeach
                        </div>
                </div>
            </div>
        <div class="clearfix"></div>
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
@stop
