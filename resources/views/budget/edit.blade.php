@extends('layouts/default')
{{-- Page title --}}
@section('title')
Update Budget
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h3>Update Budget Request</h3>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="{{ url('budget') }}">Budget Request</a></li>
        <li class="active">Update Budget Request</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
            <div class="card panel-primary ">

                <div class="card-heading">
                    <div class="pull-right">
                        <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                            Update Budget Request
                        </div>
                    </div>
                </div>

                <div class="card-body">
                <div class="row">
                    <div class="alert alert-danger-2" style="display:none"></div>
                    <div class="col-md-6">
                        <div class="form-horizontal">
                            <div class="form-group row">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="status" id="status">
                                <label class="col-md-3 text-right control-label col-form-label">Payee*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="payee_code" name="payee_code" style="width: 100%; height:36px;">
                                        <option disabled>Select a Payee</option>
                                        @foreach($payees as $payee)
                                            <option value="{{ $payee->id }}">{{ $payee->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Office*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="office" class="form-control" id="office" placeholder="Office Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label">Address*</label>
                                <div class="col-sm-9">
                                    <input type="text" name="address" class="form-control" id="address" placeholder="Address Here">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Head, Requesting Office*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="sign_code_a" name="sign_code_a" style="width: 100%; height:36px;">
                                        <option disabled>Select Head of Office</option>
                                        @foreach($payees as $sign_a)
                                            <option value="{{ $sign_a->id }}">{{ $sign_a->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 text-right control-label col-form-label">Head, Budget Division*</label>
                                <div class="col-md-9">
                                    <select class="select2 form-control custom-select" id="sign_code_b" name="sign_code_b" style="width: 100%; height:36px;">
                                        <option disabled>Select Budget Chief</option>
                                        @foreach($payees as $sign_b)
                                            <option value="{{ $sign_b->id }}">{{ $sign_b->signatory_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>

                    <div class="card-body">
                        <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="add">Add Account</a></span>

                            <table class="table table-striped table-bordered display nowrap" id="budgetdatatable">
                                <thead>
                                    <tr class="filters">
                                        <th scope="col">Responsibility Center</th>
                                        <th scope="col">Particulars</th>
                                        <th scope="col">MFO-PAP</th>
                                        <th scope="col">UACS Code</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th scope="col">Responsibility Center</th>
                                        <th scope="col">Particulars</th>
                                        <th scope="col">MFO-PAP</th>
                                        <th scope="col">UACS Code</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                            <span><a class="btn btn-info btn-sm float-left" href="javascript:void(0)" id="save">Save Budget Request</a></span>
                        </div>
                    </div>

            </div>

            <div class="modal fade" id="ajaxModel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="budgetForm" name="budgetForm" class="form-horizontal">
                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">Responsibility Center*</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="rc" name="rc" style="width: 100%; height:36px;">
                                         <option disabled>Select Responsibility Center</option>
                                            @foreach($rcs as $rc)
                                                <option value="{{ $rc->id }}">{{ $rc->rc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Particulars</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="particulars" class="form-control" id="particulars" placeholder="Particulars Here">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">MFO-PAP*</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="mfo" name="mfo" style="width: 100%; height:36px;">
                                        <option disabled>Select MFO-PAP</option>
                                            @foreach($mfos as $mfo)
                                                <option value="{{ $mfo->id }}">{{ $mfo->mfo_particulars }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-3 text-right control-label col-form-label">UACS*</label>
                                    <div class="col-md-9">
                                        <select class="select2 form-control custom-select" id="uac" name="uac" style="width: 100%; height:36px;">
                                        <option disabled>Select Account</option>
                                            @foreach($uacs as $uac)
                                                <option value="{{ $uac->id }}">{{ $uac->ac_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Total Budget*</label>
                                    <div class="col-sm-9">
                                        @foreach($totalamounts as $totalamount)
                                            <input type="number" name="totalamount" class="form-control" id="totalamount" value="{{ $totalamount->budget_total }}" disabled>
                                        @endforeach
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="fname" class="col-sm-3 text-right control-label col-form-label">Amount*</label>
                                    <div class="col-sm-9">
                                        <input type="number" name="amount" class="form-control" id="amount" placeholder="Amount Here">
                                    </div>
                                    <p> Must Check if the amount can still be accommodated from the account_codes->budget_total</p>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                </div>

                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>

        jQuery(function() {
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });

            var _jsonCh = {!! json_encode($ch->toArray()) !!};
            var _jsonChdetails = {!! json_encode($chdetails->toArray()) !!};
            var _id = '0';

            var budgetdatatable = $('#budgetdatatable').DataTable({
                select: {
                    style: 'single'
                },
                order: [[ 0, 'desc' ]],
                orderCellsTop: true
            });

            var i;
            for (i = 0; i < _jsonCh.length; i++) {
                console.log(_jsonCh[i]['id']);
                _id = _jsonCh[i]['id'];
                jQuery('#payee_code').val(_jsonCh[i]['payee_id']);
                jQuery('#office').val(_jsonCh[i]['office']);
                jQuery('#address').val(_jsonCh[i]['address']);
                jQuery('#sign_code_a').val(_jsonCh[i]['signatory_A_id']);
                jQuery('#sign_code_b').val(_jsonCh[i]['signatory_B_id']);
            }
            for (i = 0; i < _jsonChdetails.length; i++) {
                budgetdatatable.row.add( [
                    _jsonChdetails[i]['rc_description'],
                    _jsonChdetails[i]['particulars'],
                    _jsonChdetails[i]['mfo_particulars'],
                    _jsonChdetails[i]['ac_description'],
                    _jsonChdetails[i]['amount'],
                    '<td><button type="button" name="removebudget" id="removebudget" class="btn btn-danger btn-xs">Remove</button></td>'
                ] ).draw(false);
            }



            jQuery('#add').click(function () {
                jQuery('#saveBtn').val("create-account");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#budgetForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Account (Under Construction)");
                jQuery('#ajaxModel').modal('show');
                jQuery('.alert-danger').hide();
            });

            jQuery('#saveBtn').click(function () {
                $('.alert-danger').empty();
                _rc = $( "#rc option:selected" ).text()
                _particulars = $("#particulars").val();
                _mfo = $( "#mfo option:selected" ).text()
                _uac = $( "#uac option:selected" ).text()
                _totalamount = $("#totalamount").val();
                _amount = $("#amount").val();
                _action = '<td><button type="button" name="removebudget" id="removebudget" class="btn btn-danger btn-xs">Remove</button></td>';
                var _bd = budgetdatatable.rows().data();

                if(document.getElementById("rc").value == ''){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<p>Responsibility Center is required</p>');
                }else if(document.getElementById("mfo").value == ''){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<p>MFO-PAP is required</p>');
                }else if(document.getElementById("uac").value == ''){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<p>UAC is required</p>');
                }else if(document.getElementById("amount").value == ''){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<p>amount is required</p>');
                }else if(Number(_totalamount) < Number(_amount)){
                    jQuery('.alert-danger').show();
                    jQuery('.alert-danger').append('<p>You dont have enough budget</p>');
                }else{
                        jQuery('.alert-danger').hide();
                        budgetdatatable.row.add( [
                            _rc,
                            _particulars,
                            _mfo,
                            _uac,
                            _amount,
                            _action,
                        ] ).draw(false);
                }

                return false;
            });

            $(document).on('click', '#removebudget', function(){
                if(confirm("Are you sure you want to remove this?")){
                    budgetdatatable.row( $(this).parents('tr')).remove().draw();
                    console.log( 'Row index: '+budgetdatatable.row( this ).index() );
                }
            });


            jQuery('#uac').change(function () {
                var myJsonData = {id: $("#uac").val()}
                jQuery.get('/ac', myJsonData, function (data) {
                    if($.trim(data)){
                        $("#totalamount").val(data[0].budget_total);
                    }
                });
            });


            $('#save').click(function () {


                var _emp_id = '';
                var _item_id = '';
                var _received_by = '';
                var _release_by = '';
                var _payee = document.getElementById("payee_code").value;
                var _office = document.getElementById("office").value;
                var _address = document.getElementById("address").value;
                var _sign1 = document.getElementById("sign_code_a").value;
                var _sign2 = document.getElementById("sign_code_b").value;
                var _account = [];
                var _acc = budgetdatatable.rows().data();
                console.log('This is the id: '+ _id);
                for(i=0; i<_acc.length; i++){
                    _account.push( {
                                    rc : _acc[i][0],
                                    par : _acc[i][1],
                                    mfo : _acc[i][2],
                                    uac : _acc[i][3],
                                    amount : _acc[i][4]
                                    } );
                }

                var form_data = new FormData()
                form_data.append('id', _id);
                form_data.append('payee', _payee);
                form_data.append('office', _office);
                form_data.append('address', _address);
                form_data.append('sign1', _sign1);
                form_data.append('sign2', _sign2);
                form_data.append('status', 'edit');
                form_data.append('account', JSON.stringify(_account));

                if(budgetdatatable.data().count()){
                    $('.alert-danger').html('');
                    $.ajax({
                            data: form_data,
                            url: "{{ route('budget.store') }}",
                            type: "POST",
                            contentType: false, // The content type used when sending data to the server.
                            cache: false, // To unable request pages to be cached
                            processData: false,
                            success: function (data) {
                                if(data.errors){
                                    $('.alert-danger-2').html('');
                                    $('#saveBtn').html('Save changes');
                                    $.each(data.errors, function(key, value){
                                        $('.alert-danger-2').show();
                                        $('.alert-danger-2').append('<li>'+value+'</li>');
                                    });
                                }else{
                                    alert("Budget Updated!");
                                    document.getElementById("date_received").value = '';
                                    document.getElementById("date_released").value = '';
                                    document.getElementById("payee_code").value = '';
                                    document.getElementById("office").value = '';
                                    document.getElementById("office").value = '';
                                    document.getElementById("sign_code_a").value = '';
                                    document.getElementById("sign_code_b").value = '';
                                    document.getElementById("ac_code").value = '';
                                    document.getElementById("fc").value = '';
                                    document.getElementById("start").value = '';
                                    budgetdatatable.clear().draw();
                                    $('.alert-danger').hide();
                                    console.log(data);
                                }
                            },
                            error: function (data) {
                                console.log('Error:', data);
                                jQuery('#saveBtn').html('Save Changes');
                            }
                    });
                }else{
                    if(!budgetdatatable.data().count()){
                            $('.alert-danger-2').show();
                            $('.alert-danger-2').append('<li>The account is required.</li>');
                    }
                }


            });


        });

    </script>
@stop
