<div class="card-body">
    <div class="form-group row">
        <label for="fname" class="col-sm-3 text-right control-label col-form-label">MFO-PAP Code</label>
        <div class="col-sm-9">
            <input type="text" name="mfo_code" value="{{ old('mfo_code') ?? $prc->mfo_code }}" class="form-control" id="fname" placeholder="MFO-PAP Code Here">
        </div>
    </div>

    <div class="form-group row">
        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Particulars</label>
        <div class="col-sm-9">
            <input type="text" name="mfo_particulars" value="{{ old('mfo_particulars') ?? $prc->mfo_particulars }}" class="form-control" id="lname" placeholder="MFO-PAP Particulars Here">
        </div>
    </div>
</div>

@csrf
