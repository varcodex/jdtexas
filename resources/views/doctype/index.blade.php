@extends('layouts/default')
{{-- Page title --}}
@section('title')
Document Types
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datatables/css/dataTables.bootstrap4.css') }}" />
<link href="{{ asset('css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
@stop


{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>Document Types</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                Dashboard
            </a>
        </li>
        <li><a href="#"> Document Types</a></li>
        <li class="active"> List</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="col-12">
        <div class="card panel-primary ">
            <div class="card-heading">
            <span><a class="btn btn-success btn-sm float-left" href="javascript:void(0)" id="createNewDTCode">New Document Type</a></span>   
            <div class="pull-right">
            <div class="card-title pull-right"> <i class="livicon" data-name="list" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                    Type List
            </div>
            </div>
            </div>
            <div class="card-body">
                <div class="table-responsive-lg table-responsive-sm table-responsive-md">
                <table class="table table-bordered width100" id="dtdatatable">
                                <thead>
                                    <tr class="filters">
                                        <th>#</th>
                                        <th width="150px">Code</th>
                                        <th width="150px">Particulars</th>
                                        <th width="150px"> Purpose</th>
                                        <th> Category</th>
                                        <th>Office</th>
                                        <th> Generated</th>
                                        <th> Last Update</th>
                                        <th width="150px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>#</th>
                                        <th>Code</th>
                                        <th>Particulars</th>
                                        <th>Purpose</th>
                                        <th>Category</th>
                                        <th>Office</th>
                                        <th >Generated</th>
                                        <th >Last Update</th>
                                        <th width="150px">Action</th>
                                    </tr>
                                </tfoot>
                </table>
                </div>
            </div>
        </div>

            <div class="modal fade" id="ajaxModelDT" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="alert alert-danger" style="display:none"></div>
                        <div class="modal-header">
                            <h4 class="modal-title" id="modelHeading"></h4>
                        </div>


                        <div class="card-body">
                            <form id="dtForm" name="dtForm" class="form-horizontal">
                                <input type="hidden" name="id" id="id">
                                <input type="hidden" name="status" id="status">

                                <div class="form-group row">
                                    <label for="doc_code" class="col-sm-3 text-right control-label col-form-label">Code*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="doc_code" class="form-control" id="doc_code" placeholder="Document Code Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="Particulars" class="col-sm-3 text-right control-label col-form-label">Particulars*</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="particulars" class="form-control" id="particulars" placeholder="Particulars Here">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="purpose_id" class="col-sm-3 text-right control-label col-form-label">Purpose*</label>
                                    <div class="col-sm-9">
                                        <select class="select2 form-control custom-select" id="purpose_id" name="purpose_id" style="width: 100%; height:36px;">
                                         <option disabled>Select Document Purpose</option>
                                            @foreach($purposes as $purpose)
                                                <option value="{{ $purpose->id }}">{{ $purpose->purpose }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="category" class="col-sm-3 text-right control-label col-form-label">Category</label>
                                    <div class="col-sm-9">
                                        <select class="select2 form-control custom-select" id="category" name="category" style="width: 100%; height:36px;">
                                            <option disabled>Select category</option>
                                            <option value="Internal">Internal</option>
                                            <option value="External">External</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="rc_id" class="col-sm-3 text-right control-label col-form-label">Responsibility Center*</label>
                                    <div class="col-sm-9">
                                        <select class="select2 form-control custom-select" id="rc_id" name="rc_id" style="width: 100%; height:36px;">
                                            <option value="0">For All Offices</option>
                                            @foreach($rcs as $rc)
                                                <option value="{{ $rc->id }}">{{ $rc->rc_description }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary" id="saveBtn" value="create">Save changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('vendors/datatables/js/dataTables.bootstrap4.js') }}" ></script>
    <script>
        jQuery(function(){
            jQuery.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
        });
            
        var table = jQuery('#dtdatatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    'url':"{{ route('doctype.index') }}",
                    'type': 'GET',
                    'headers': {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                orderCellsTop: true,
                fixedHeader: true,
                columns: [
                        {data: 'id', name: 'id'},
                        {data: 'doc_code', name: 'doc_code'},
                        {data: 'particulars', name: 'particulars'},
                        {data: 'purpose', name: 'purpose'},
                        {data: 'category', name: 'category'},
                        {data: 'rc_description', name: 'rc_description'},
                        {data: 'created_at', name: 'created_at'},
                        {data: 'updated_at', name: 'updated_at'},
                        {data: 'action', name: 'action', orderable: false, searchable: false},
                    ],
            order: [[0, 'desc']]
            });


            jQuery('#createNewDTCode').click(function () {
                jQuery('#saveBtn').val("create-prccode");
                jQuery('#id').val('');
                jQuery('#status').val('create');
                jQuery('#dtForm').trigger("reset");
                jQuery('#modelHeading').html("Create New Document Type");
                jQuery('#ajaxModelDT').modal('show');
                jQuery('.alert-danger').hide();
            });

            jQuery('body').on('click', '.editDT', function () {
                var id = jQuery(this).data('id');
                jQuery.get("{{ route('doctype.index') }}" +'/' + id +'/edit', function (data) {
                    jQuery('#modelHeading').html("Edit Document Type");
                    jQuery('#saveBtn').val("edit-doctype");
                    jQuery('#status').val('edit');
                    jQuery('#ajaxModelDT').modal('show');                   
                    jQuery('#saveBtn').html('Save changes');
                    jQuery('#id').val(data.id);
                    jQuery('#doc_code').val(data.doc_code);
                    jQuery('#particulars').val(data.particulars);
                    jQuery('#purpose_id').val(data.purpose_id);
                    jQuery('#category').val(data.category);
                    jQuery('#rc_id').val(data.rc_id);
                })
            });

            jQuery('#saveBtn').click(function (e) {
                e.preventDefault();
                jQuery(this).html('Sending..');

                jQuery.ajax({
                data: jQuery('#dtForm').serialize(),
                url: "{{ route('doctype.store') }}",
                type: "POST",
                dataType: 'json',
                success: function (data) {
                    if(data.errors){
                  		jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                  		jQuery.each(data.errors, function(key, value){
                  			jQuery('.alert-danger').show();
                  			jQuery('.alert-danger').append('<li>'+value+'</li>');
                  		});

                  	}else if(data.code == "1"){
                        jQuery('.alert-danger').html('');
                        jQuery('#saveBtn').html('Save changes');
                        jQuery('.alert-danger').show();
                        jQuery('.alert-danger').append('<li>Document Code Already Exists</li>');
                    }else{
                        jQuery('#dtForm').trigger("reset");
                        jQuery('#ajaxModelDT').modal('hide');
                        table.draw();
                  	}

                },
                error: function (data) {
                    console.log('Error:', data);
                    $.alert({
                        theme: 'dark',
                        title: 'Missing Entry!',
                        content: 'Required input must not be blank.',
                    });
                    jQuery('#saveBtn').html('Save Changes');
                }
                });
            });

            jQuery('body').on('click', '.deleteDTCode', function () {
                    var id = jQuery(this).data("id");
                    $.confirm({
                        theme: 'dark',
                        title: 'Please Confirm!',
                        content: 'Are you sure you want to delete this record?',
                        buttons: {
                            confirm: function () {
                                $.ajax({
                                    type: "GET",
                                    url: "{{ route('doctype.index') }}" +'/' + id +'/remove',
                                    success: function(data){
                                        table.draw();
                                    },
                                    error: function(data){
                                        console.log('Error', data);
                                    }
                                });

                            },
                            cancel: function () {
                               // $.alert('Canceled!');
                            }
                        }
                    });               
            });

    </script>

@stop