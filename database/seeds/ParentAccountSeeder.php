<?php

use Illuminate\Database\Seeder;

class ParentAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parent_account_codes')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`parent_account_codes` (`id`, `pac_code`, `pac_description`,`ac_id`, `is_deleted`,`created_at`) VALUES
        (1,'001','Salaries and Wages','1','0','2020-03-01'),
        (2,'002','Honoraria','1','0','2020-03-01'),
        (3,'003','Travelling Expense','10','0','2020-03-01'),
        (4,'004','Training and Scholarship Expense','10','0','2020-03-01'),
        (5,'005','Supplies and Material EXpense','10','0','2020-03-01'),
        (6,'006','Utility Expenses','10','0','2020-03-01'),
        (7,'007','Communication Expenses','10','0','2020-03-01'),
        (8,'008','Survey, Research, Exploration, and Devt Expense','10','0','2020-03-01'),
        (9,'009','Professonal Services','10','0','2020-03-01'),
        (10,'010','Other Professional Services','10','0','2020-03-01'),
        (11,'011','General Services','10','0','2020-03-01'),
        (12,'012','Repair and Maintenance','10','0','2020-03-01'),  
        (13,'013','Financial Assistance to Researchers','10','0','2020-03-01'),
        (14,'014','Labor and Wages','10','0','2020-03-01'),
        (15,'015','Other Maintenance and Operating Expenses','2','0','2020-03-01'),
        (16,'016','Other Land Improvements','76','0','2020-03-01'),
        (17,'017','Other Structures','76','0','2020-03-01'),
        (18,'018','Machinery and Equipment','76','0','2020-03-01'),
        (19,'019','Office Equipment','76','0','2020-03-01'),
        (20,'020','Information and Communication Tech Equipment','76','0','2020-03-01'),
        (21,'021','Agricultural and Forestry Equipment','76','0','2020-03-01'),
        (22,'022','Medical Equipment','76','0','2020-03-01'),
        (23,'023','Technical and Scientific Equipment','76','0','2020-03-01'),
        (24,'024','Transportation Equipment','76','0','2020-03-01'),
        (25,'025','Furniture Fixtures and Books','76','0','2020-03-01'), 
        (26,'026','Other Property Plant & Equipment','76','0','2020-03-01');";
        DB::unprepared($statement);
    }
}
