<?php

use Illuminate\Database\Seeder;

class AllotmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('allotment_classes')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`allotment_classes` (`id`, `uacs_code`, `uacs_accronym`, `uacs_particulars`, `is_deleted`,`created_at`) VALUES
        (1,'01', 'PS', 'Personnel Services','0','2020-03-01'),
        (10,'02', 'MOOE', 'Maintenance and Other Operating Expenses','0','2020-03-01'),
        (30,'03', 'FE', 'Financial Expenses','0','2020-03-01'),
        (40,'04', 'DC', 'Direct Costs -manufacturing and trading','0','2020-03-01'),
        (50,'05', 'NCE', 'Non-Cash Expenses','0','2020-03-01'),
        (76,'06', 'CO', 'Capital Outlays','0','2020-03-01');";
        DB::unprepared($statement);
    }
}
