<?php

use Illuminate\Database\Seeder;

class FundClusterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fund_clusters')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`fund_clusters` (`id`, `fc_code`, `fc_particulars`, `is_deleted`,`created_at`) VALUES
        (1,'101101','General Fund','0','2020-03-01'),
        (2,'102102','General Fund - Continuing','0','2020-03-01'),
        (3,'206441','Income','0','2020-03-01'),
        (4,'206442','Income - Unexpended','0','2020-03-01'),
        (5,'308601','Trust Fund','0','2020-03-01'),
        (6,'207511','Revolving Fund','0','2020-03-01');";
        DB::unprepared($statement);
    }
}
