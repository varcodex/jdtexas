<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`roles` (`id`, `slug`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
        (1, 'admin', 'Admin', '{\"admin\":1}', '2020-03-15 02:51:56', '2020-03-15 02:51:56'),
        (2, 'user', 'User', NULL, '2020-03-15 02:51:56', '2020-03-15 02:51:56'),
        (3, 'budaid', 'Budget Aid', '{\"admin\":1}', '2020-03-15 02:54:01', '2020-03-15 02:54:01'),
        (4, 'budoff', 'Budget Officer', '{\"admin\":1}', '2020-03-15 02:54:35', '2020-03-15 02:54:35'),
        (5, 'accounting', 'Accounting', '{\"admin\":1}', '2020-03-15 02:54:35', '2020-03-15 02:54:35'),
        (6, 'cashier', 'Cashier', '{\"admin\":1}', '2020-03-15 02:54:59', '2020-03-15 02:54:59');";  
        DB::unprepared($statement);
    }
}
