<?php

use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;

class MISSeeder extends DatabaseSeeder 
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users'); // Using truncate function so all info will be cleared when re-seeding.
		DB::table('activations');
		$budget = Sentinel::registerAndActivate(array(
			'email'       => 'budaid@admin.com',
			'password'    => "123456",
			'first_name'  => 'budget',
			'last_name'   => 'aid',
			'gender'	  => 'female',
			'pic'	      => 'avatar1.jpg',
			'rc_id'		  => '14',
        )
        );
	
		
		$budoff = Sentinel::registerAndActivate(array(
			'email'       => 'budoff@admin.com',
			'password'    => "123456",
			'first_name'  => 'budget',
			'last_name'   => 'officer',
			'gender'	  => 'female',
			'pic'	      => 'avatar2.jpg',
			'rc_id'		  => '14',
        )
        );

        $accounting = Sentinel::registerAndActivate(array(
			'email'       => 'accounting@admin.com',
			'password'    => "123456",
			'first_name'  => 'accounting',
			'last_name'   => 'officer',
			'gender'	  => 'female',
			'pic'	      => 'avatar3.jpg',
			'rc_id'		  => '13',
		)
        );

        $cashier = Sentinel::registerAndActivate(array(
			'email'       => 'cashier@admin.com',
			'password'    => "123456",
			'first_name'  => 'university',
			'last_name'   => 'cashier',
			'gender'	  => 'male',
			'pic'	      => 'avatar4.jpg',
			'rc_id'		  => '18',
        )
        );

        $staff = Sentinel::registerAndActivate(array(
			'email'       => 'staff@admin.com',
			'password'    => "123456",
			'first_name'  => 'office',
			'last_name'   => 'staff',
			'gender'	  => 'female',
			'pic'	      => 'avatar5.jpg',
			'rc_id'		  => '47',
        )
        );

        $vpad = Sentinel::registerAndActivate(array(
			'email'       => 'vpad@admin.com',
			'password'    => "123456",
			'first_name'  => 'Vice President',
			'last_name'   => 'Administration',
			'gender'	  => 'male',
			'pic'	      => 'avatar6.jpg',
			'rc_id'		  => '3',
        )
        );

		$vpaa = Sentinel::registerAndActivate(array(
			'email'       => 'vpaa@admin.com',
			'password'    => "123456",
			'first_name'  => 'Vice President',
			'last_name'   => 'Academic Affairs',
			'gender'	  => 'female',
			'pic'	      => 'avatar6.jpg',
			'rc_id'		  => '25',
        )
		);
		
		$vpre = Sentinel::registerAndActivate(array(
			'email'       => 'vprea@admin.com',
			'password'    => "123456",
			'first_name'  => 'Vice President',
			'last_name'   => 'Research and Extension',
			'gender'	  => 'female',
			'pic'	      => 'avatar6.jpg',
			'rc_id'		  => '41',
        )
		);
				
        $oup = Sentinel::registerAndActivate(array(
			'email'       => 'president@admin.com',
			'password'    => "123456",
			'first_name'  => 'Raul',
			'last_name'   => 'Bradecina',
			'gender'	  => 'male',
			'pic'	      => 'avatar7.jpg',
			'rc_id'		  => '2',
        )
        );		

        $coa = Sentinel::registerAndActivate(array(
			'email'       => 'hrmo@admin.com',
			'password'    => "123456",
			'first_name'  => 'Human Resource',
			'last_name'   => 'Office',
			'gender'	  => 'female',
			'pic'	      => 'avatar8.jpg',
			'rc_id'		  => '16',
        )
		);	

        $cas = Sentinel::registerAndActivate(array(
			'email'       => 'cas@admin.com',
			'password'    => "123456",
			'first_name'  => 'CAS',
			'last_name'   => 'Office',
			'gender'	  => 'female',
			'pic'	      => 'avatar9.jpg',
			'rc_id'		  => '29',
        )
		);			

        $cbm = Sentinel::registerAndActivate(array(
			'email'       => 'cbm@admin.com',
			'password'    => "123456",
			'first_name'  => 'CBM',
			'last_name'   => 'Office',
			'gender'	  => 'female',
			'pic'	      => 'avatar10.jpg',
			'rc_id'		  => '28',
        )
		);	
		
		$cbm = Sentinel::registerAndActivate(array(
			'email'       => 'dco@admin.com',
			'password'    => "123456",
			'first_name'  => 'Document Control',
			'last_name'   => 'Office',
			'gender'	  => 'female',
			'pic'	      => 'avatar11.jpg',
			'rc_id'		  => '47',
        )
		);

		$rsh = Sentinel::registerAndActivate(array(
			'email'       => 'rsh@admin.com',
			'password'    => "123456",
			'first_name'  => 'Research',
			'last_name'   => 'Office',
			'gender'	  => 'female',
			'pic'	      => 'avatar12.jpg',
			'rc_id'		  => '44',
        )
		);


		$this->command->info('Users created with password 123456');
    }

}
