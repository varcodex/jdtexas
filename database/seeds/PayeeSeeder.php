<?php

use Illuminate\Database\Seeder;

class PayeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payees')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`payees` (`id`, `signatory_name`, `signatory_designation`,`address`,`email_address`, `rc_id`, `is_signatory`,  `is_deleted`, `created_at`) VALUES
            (1, 'Raul G. Bradecina', 'SUC President', 'Tigaon, Camarines Sur','jcapucao265@gmail.com', '2','1', '0', '2020-03-01'),
            (2, 'Arnel B.Zarcedo', 'VP Administration and Finance', 'Goa, Camarines Sur','naykss@gmail.com', '3','1', '0', '2020-03-01'),
            (3, 'Amalia S. Ocampo', 'Chief Budget Officer', 'Goa, Camarines Sur','naykss@gmail.com', '14','1', '0', '2020-03-01'),
            (4, 'Joni Neil B. Capucao', 'Associate Professor I', 'Goa, Camarines Sur','jcapucao265@gmail.com', '29', '0','0', '2020-03-01');";
        DB::unprepared($statement);
    }
}
