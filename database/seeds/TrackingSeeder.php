<?php

use Illuminate\Database\Seeder;

class TrackingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('track_documents')->truncate();
        DB::table('purposes')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`purposes` (`id`,  `purpose`, `is_deleted`,`created_at`) VALUES
            (1, 'appropriate action','0','2020-03-01'),
            (2, 'coding / deposit / preparation of receipt','0','2020-03-01'),
            (3, 'comment / reaction / response','0','2020-03-01'),
            (4, 'compliance / implementation','0','2020-03-01'),
            (5, 'dissemination of information','0','2020-03-01'),
            (6, 'draft of reply','0','2020-03-01'),
            (7, 'endorsement / recommendation','0','2020-03-01'),
            (8, 'follow-up','0','2020-03-01'),
            (9, 'investigation / verification / report', '0','2020-03-01'),
            (10, 'notation / file','0','2020-03-01'),
            (11, 'notification / reply to party','0','2020-03-01'),
            (12, 'study and report to','0','2020-03-01'),
            (13, 'translation','0','2020-03-01'),
            (14, 'your information','0','2020-03-01');";
        DB::unprepared($statement);
          
    }
}
