<?php

use Illuminate\Database\Seeder;

class ClaimsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('claims_headers')->truncate();
        DB::table('claims_details')->truncate();
    }
}
