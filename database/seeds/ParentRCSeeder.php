<?php

use Illuminate\Database\Seeder;

class ParentRCSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parent_responsibility_centers')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`parent_responsibility_centers` (`id`, `prc_code`, `prc_description`, `is_deleted`,`created_at`) VALUES
            (1, 'GOA', '08-056-0100001-000','0','2020-03-01'),
            (2, 'SAGNAY', '08-056-0200002-000','0','2020-03-01'),
            (3, 'SANJOSE', '08-056-0300003-000','0','2020-03-01'),
            (4, 'CARAMOAN', '08-056-0400004-000','0','2020-03-01'),
            (5, 'LAGONOY', '08-056-0500005-000','0','2020-03-01'),
            (6, 'SALOGON', '08-056-0600006-000','0','2020-03-01'),
            (7, 'TINAMBAC', '08-056-0700007-000','0','2020-03-01'),
            (8, 'SHS', '08-056-0100002-000','0','2020-03-01');";
        DB::unprepared($statement);
    }
}
