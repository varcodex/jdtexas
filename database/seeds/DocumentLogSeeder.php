<?php

use Illuminate\Database\Seeder;

class DocumentLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('document_logs')->truncate();
    }
} 
