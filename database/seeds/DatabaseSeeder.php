<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminSeeder::class);
        $this->call(RoleUsersSeeder::class);
        $this->call(MISSeeder::class);
        $this->call(FundClusterSeeder::class);
        $this->call(MFOPAPSeeder::class);
        $this->call(ParentRCSeeder::class);
        $this->call(RCSeeder::class);
        $this->call(AllotmentSeeder::class);
        $this->call(ParentAccountSeeder::class);
        $this->call(AccountSeeder::class);
        $this->call(PayeeSeeder::class);
        $this->call(DocTypeSeeder::class);
        $this->call(DocumentLogSeeder::class);
        $this->call(RolesSeeder::class);
        $this->call(TrackingSeeder::class);
        $this->call(ClaimsSeeder::class);
    }
}
