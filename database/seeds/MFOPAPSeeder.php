<?php

use Illuminate\Database\Seeder;

class MFOPAPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mfo_paps')->truncate();
        $statement = "INSERT INTO ".env('DB_PREFIX')."`mfo_paps` (`id`,  `mfo_particulars`, `mfo_code`, `is_deleted`,`created_at`) VALUES
            (1, 'GASS', '100010000','0','2020-03-01'),
            (2, 'STO', '200010000','0','2020-03-01'),
            (3, 'HES', '301000000','0','2020-03-01'),
            (4, '4PS', '301010000','0','2020-03-01'),
            (5, 'ADVANCE', '302010000','0','2020-03-01'),
            (6, 'RESEARCH', '303010000','0','2020-03-01'),
            (7, 'EXTENSION', '304010000','0','2020-03-01');";
        DB::unprepared($statement);
    }
}
