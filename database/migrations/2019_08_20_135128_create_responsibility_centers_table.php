<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResponsibilityCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responsibility_centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('prc_id')->nullable();
            $table->string('rc_code');
            $table->string('rc_description');
            $table->string('is_deleted')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
