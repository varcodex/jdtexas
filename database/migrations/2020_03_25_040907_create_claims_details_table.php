<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('claim_header_id');
            $table->string('rc_id');
            $table->string('particulars');
            $table->string('mfo_id');
            $table->string('uacs_id');
            $table->double('amount');
            $table->string('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims_details');
    }
}
