<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims_headers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('barcode')->nullable();
            $table->string('allotment_id');
            $table->string('fund_cluster_id');
            $table->string('year');
            $table->string('month');
            $table->string('payee_id');
            $table->string('office');
            $table->string('address');
            $table->string('signatory_A_id');
            $table->string('signatory_B_id');
            $table->string('user_id');
            $table->string('status');
            $table->string('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims_headers');
    }
}
