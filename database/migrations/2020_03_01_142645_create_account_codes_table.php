<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pac_id')->nullable();
            $table->unsignedBigInteger('ac_id')->nullable();
            $table->string('ac_code');
            $table->string('ac_description');
            $table->string('approved_budget');
            $table->string('budget_adjustment');
            $table->string('budget_total');
            $table->string('is_deleted')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_codes');
    }
}
