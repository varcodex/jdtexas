<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tracking_no');
            $table->string('type');
            $table->string('title');
            $table->string('purpose_id');
            $table->string('remarks')->nullable();
            $table->string('office_action');
            $table->string('orig_office');
            $table->string('current_office');
            $table->string('payee_id')->nullable();
            $table->string('user_id');
            $table->string('track_status');
            $table->timestamps();
        });
    }

     /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_logs');
    }
}
