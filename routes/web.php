<?php
use App\InformUsers;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
/*
use App\Http\Controllers\HomeController;
use App\Mail\ContactMail;
use illuminate\http\Request;

include_once 'web_builder.php';
*/
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
Clear Cache Commands
php artisan route:cache
php artisan cache:clear
php artisan config:cache
php artisan view:clear
php artisan optimize

*/

Route::pattern('slug', '[a-z0-9- _]+');

Route::get("/email", function() {
    Mail::raw('Now I know how to send emails with Laravel', function($message)
     {
         $message->subject('Hi There!!');
         $message->from(config('mail.from.address'), config("app.name"));
         $message->to('jbcapucao265@gmail.com');
     });
 });


//PRC
Route::resource('parentresponsibilitycenter','PRCController');
Route::get('parentresponsibilitycenter/{id}/remove',
            ['uses' => 'PRCController@deletePRC'
            ])->name('parentresponsibilitycenter.remove');

//Allotment Class
Route::resource('allotmentclass','AllotmentClassController');
Route::get('allotmentclass/{id}/remove',
            ['uses' => 'AllotmentClassController@deleteAC'
            ])->name('allotmentclass.remove');

//Funding Cluster
Route::resource('fundcluster','FundClusterController');
Route::get('fundcluster/{id}/remove',
            ['uses' => 'FundClusterController@deleteFC'
            ])->name('fundcluster.remove');

//MFO-PAP
Route::resource('mfopap','MfoPapController');
Route::get('mfopap/{id}/remove',
            ['uses' => 'MfoPapController@deleteMP'
            ])->name('mfopap.remove');
//RC
Route::resource('responsibilitycenter','RCController');
Route::get('responsibilitycenter/{id}/remove',
            ['uses' => 'RCController@deleteRCCode'])->name('rc.remove');

//Payee
Route::resource('payee','PayeeController');
Route::get('payee/{id}/remove',
            ['uses' => 'PayeeController@deletePayee'])->name('payee.remove');

//PAC
Route::resource('parentaccountcode','ParentAccountCodeController');
Route::get('parentaccountcode/{id}/remove',
            ['uses' => 'ParentAccountCodeController@deletePAC'])->name('pac.remove');

//AC
Route::resource('accountcode','AccountCodeController');
Route::get('accountcode/{id}/remove',
            ['uses' => 'AccountCodeController@deleteACCode'])->name('ac.remove');
Route::get('reports',
            ['uses' => 'AccountCodeController@printPDF'])->name('printPDF');


//Budget Request
Route::resource('budget','ClaimsHeaderController');
Route::get('/ac', 'ClaimsHeaderController@ac');
Route::get('/printBurs/{id}',
            ['uses' => 'ClaimsHeaderController@bursPdf']);


# jbc addeds
Route::resource('released','ReleasedController');
Route::resource('received','ReceivedController');
Route::get('onroute','TrackDocumentController@onroute')->name('onroute');
Route::get('vfiled','TrackDocumentController@vfiled')->name('vfiled');

Route::resource('documentstracking','DocumentsTrackingController');
Route::get('documentstracking/{id}/remove',
            ['uses' => 'DocumentsTrackingController@deleteDoc'])->name('documentstracking.remove');

Route::resource('doctype','DocumentTypesController');
Route::resource('purpose','PurposesController');
Route::get('purpose/{id}/remove',
            ['uses' => 'PurposesController@deleteDTCode'])->name('purpose.remove');

//notification of
Route::get('informuser','InformUserController@index');
Route::post('informuser','InformUserController@store')->name('informuser');
Route::get('informuser/{id}','InformUserController@update')->name('informuser');

/*view Compose*/
View::composer(['*'], function($view) {
    if($userInfos = Sentinel::check()){
        $offices = DB::table('users')
        ->join('responsibility_centers', 'responsibility_centers.id', '=', 'users.rc_id')
        ->select('responsibility_centers.rc_description as rc_description')
        ->where('users.id', $userInfos->id)
        ->get();
        $notifs = InformUsers::where('to_rc_id', $userInfos->rc_id)->whereNull('is_read')->get();
        $allnotifs = InformUsers::where('to_rc_id', $userInfos->rc_id)->orderBy('created_at','desc')->get();
        $view->with('notifs', $notifs)->with('userInfo',$userInfos)
        ->with('offices', $offices)
        ->with('allnotifs',$allnotifs);
    }
});

Route::get('/', 'HomeController@showHome')->name('home');

Route::get('aboutus','HomeController@showAboutUs')->name('aboutus');

Route::get('contact','ContactFormController@create')->name('contact');
Route::post('contact','ContactFormController@store')->name('contact');

Route::get('timeline','HomeController@showTimeline')->name('timeline');
Route::get('timeline/trace','DocumentLogController@index')->name('trace');


// VuePress Documentation Route
Route::get('/docs/{file?}', 'VueControllerForm@index')->name('docs.index');

# Lock screen
Route::get('{id}/lockscreen', 'UsersController@lockscreen')->name('lockscreen');
Route::post('{id}/lockscreen', 'UsersController@postLockscreen')->name('lockscreen');

# All basic routes defined here
Route::get('login', 'AuthController@getSignin')->name('login');
Route::get('signin', 'AuthController@getSignin')->name('signin');
Route::post('signin', 'AuthController@postSignin')->name('postSignin');
Route::post('signup', 'AuthController@postSignup')->name('signup');
Route::post('forgot-password', 'AuthController@postForgotPassword')->name('signup');

# Forgot Password Confirmation
Route::get('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm')->name('forgot-password-confirm');
Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@getForgotPasswordConfirm');

# Logout
Route::get('logout', 'AuthController@getLogout')->name('logout');

# Account Activation
Route::get('activate/{userId}/{activationCode}', 'AuthController@getActivate')->name('activate');
//});


Route::group([ 'middleware' => 'admin'], function () {
    # Dashboard / Index
    Route::get('index', 'JoshController@showHome')->name('dashboard');
    Route::post('store', 'JoshController@showStore')->name('dashboard.store');

    # crop demo
    Route::post('crop_demo', 'JoshController@crop_demo')->name('crop_demo');
    # Activity log
    Route::get('activity_log', 'JoshController@ActivityLog')->name('activity_log');
    Route::get('task/data', 'TaskController@data')->name('data');
    # User Management

    Route::group([ 'prefix' => 'users'], function () {
        Route::get('data', 'UsersController@data')->name('users.data');
        Route::get('{user}/delete', 'UsersController@destroy')->name('users.delete');
        Route::get('{user}/confirm-delete', 'UsersController@getModalDelete')->name('users.confirm-delete');
        Route::get('{user}/restore', 'UsersController@getRestore')->name('restore.user');
        Route::post('{user}/passwordreset', 'UsersController@passwordreset')->name('passwordreset');
    });
    Route::resource('users', 'UsersController');

    Route::get('deleted_users',['before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'])->name('deleted_users');

    # Group Management
    Route::group(['prefix' => 'groups'], function () {
        Route::get('{group}/delete', 'GroupsController@destroy')->name('groups.delete');
        Route::get('{group}/confirm-delete', 'GroupsController@getModalDelete')->name('groups.confirm-delete');
        Route::get('{group}/restore', 'GroupsController@getRestore')->name('groups.restore');
    });
    Route::resource('groups', 'GroupsController');

    Route::get('{name?}', 'JoshController@showView');

});

# Remaining pages will be called from below controller method
# in real world scenario, you may be required to define all routes manually

Route::get('activate/{userId}/{activationCode}','FrontEndController@getActivate')->name('activate');
Route::get('forgot-password','FrontEndController@getForgotPassword')->name('forgot-password');
Route::post('forgot-password', 'FrontEndController@postForgotPassword');

# Forgot Password Confirmation
Route::post('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@postForgotPasswordConfirm');
Route::get('forgot-password/{userId}/{passwordResetCode}', 'FrontEndController@getForgotPasswordConfirm')->name('forgot-password-confirm');

