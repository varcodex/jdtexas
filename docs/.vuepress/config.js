module.exports = {
  base: '/docs/',
  dest: 'public/docs',
  title:'DTExAS Documentation',
  description:'Document Tracking and Aggregating of EXpenses System',
  head: [
    ['link', { rel: 'icon', href: 'https://picsum.photos/100' }]
  ],
  themeConfig: {
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Overview', link: '/getstarted/' }
    ],
    sidebar: [
      ['/', 'Home'],
      {
        title: 'Getting Started',
        collapsable: false,
        children: [
          ['/getstarted/', 'Install it title']
        ]
      }
    ]
  }
}