<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountCode extends Model
{
    protected $guarded = [];
    public $table = "account_codes";
    protected $fillable = ['id', 'pac_id', 'ac_id', 'ac_code', 'ac_description',
                            'approved_budget', 'budget_adjustment',
                            'budget_total', 'is_deleted', 'created_at', 'updated_at'];
        
    public function category(){
        return $this->belongsTo('App\AccuntCode','ac_id');
   
    }

    public function subcategory(){
        return $this->hasMany('App\AccountCode','ac_id');
    
    }

    

}
