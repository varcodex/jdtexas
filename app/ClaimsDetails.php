<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimsDetails extends Model
{
    protected $guarded = [];
    public $table = "claims_details";
    protected $fillable = ['id', 'claim_header_id', 'rc_id','particulars', 'mfo_id', 'uacs_id','amount',
            'is_deleted', 'created_at', 'updated_at'];
}
