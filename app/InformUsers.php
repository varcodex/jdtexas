<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InformUsers extends Model
{
    protected $guarded = [];
    public $table = "inform_users";
    protected $fillable = ['id', 'from_rc_id', 'to_rc_id', 'message',  'is_read', 'created_at', 'updated_at'];
}
