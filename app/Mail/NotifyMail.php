<?php

namespace App\Mail;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyMail extends Mailable
{
    use Queueable, SerializesModels;

    public $username, $email,$tracking_no;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($username, $email,$tracking_no)
    {
        $this->username = $username;
        $this->email= $email;
        $this->tracking_no = $tracking_no;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('DTExAS Mail')
        ->to($this->email)
        ->from('dtexas.dit.2019@gmail.com', $this->username, $this->tracking_no)
        ->markdown('emails.notifymail');
    }
}
