<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payee extends Model
{
    protected $guarded = [];
    public $table = "payees";
    protected $fillable = ['id', 'signatory_name', 'signatory_designation','address', 'email_address',
                            'rc_id','is_signatory', 'is_deleted', 'created_at', 'updated_at'];
}
