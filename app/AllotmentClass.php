<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllotmentClass extends Model
{
    protected $guarded = [];
    public $table = "allotment_classes";
    protected $fillable = ['id', 'uacs_code', 'uacs_accronym', 'uacs_particulars',
                            'is_deleted', 'created_at', 'updated_at'];
}
