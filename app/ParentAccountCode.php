<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentAccountCode extends Model
{
    protected $guarded = [];
    public $table = "parent_account_codes";
    protected $fillable = ['id', 'pac_code', 'pac_description', 'ac_id',
                            'is_deleted', 'created_at', 'updated_at'];
}
