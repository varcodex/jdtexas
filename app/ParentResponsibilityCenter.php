<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\ResponsibilityCenter;

class ParentResponsibilityCenter extends Model
{

    protected $guarded = [];
    public $table = "parent_responsibility_centers";
    protected $fillable = ['id', 'prc_code', 'prc_description',
                            'is_deleted', 'created_at', 'updated_at'];
    
}
