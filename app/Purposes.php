<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purposes extends Model
{
    protected $guarded = [];
    public $table = "purposes";
    protected $fillable = ['id', 'purpose','created_at', 'updated_at', 'is_deleted'];
}
