<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentTypes extends Model
{
    protected $guarded = [];
    public $table = "doc_type";
    protected $fillable = ['id', 'doc_code', 'particulars','purpose_id', 'category', 'rc_id', 'is_deleted', 'created_at', 'updated_at'];
    
}
