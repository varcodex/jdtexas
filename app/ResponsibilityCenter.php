<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResponsibilityCenter extends Model
{
    protected $guarded = [];
    public $table = "responsibility_centers";
    protected $fillable = ['id', 'prc_id', 'rc_code', 'rc_description',
                            'is_deleted', 'created_at', 'updated_at'];
    
}
