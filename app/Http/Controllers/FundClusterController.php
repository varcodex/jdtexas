<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\FundCluster;
use Session;
use DataTables;


class FundClusterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FundCluster::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editFCCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteFCCode">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('fundings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'fc_code' => 'required|min:1',
            'fc_particulars' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(FundCluster::where('fc_code', '=', Input::get('fc_code'))->exists()){
                return response()->json(['code'=>'1']);
            }else{
                FundCluster::updateOrCreate(['id' => $request->id],
                                ['fc_code' => $request->fc_code, 'fc_particulars' => $request->fc_particulars,
                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(FundCluster::where([
                ['fc_code', '=', Input::get('fc_code')],
                ['id', '=', Input::get('id')],
                ['is_deleted', '<>', '1']
                ])->exists()){
                    FundCluster::find(Input::get('id'))
                            ->update(['fc_particulars' => $request->fc_particulars,
                                    'fc_code' => $request->fc_code,
                                    'is_deleted' => '0']);
            }else{
                if(FundCluster::where([
                                    ['fc_code', '=', Input::get('fc_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['fc_code'=>'1']);
                }else{
                    FundCluster::find(Input::get('id'))->update(
                                    ['fc_particulars' => $request->fc_particulars,
                                    'fc_code' => $request->fc_code,
                                    'is_deleted' => '0']);
                }

            }
        }

        return response()->json(['success'=>'Funding Source saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FundCluster  $fundCluster
     * @return \Illuminate\Http\Response
     */
    public function show(FundCluster $fundCluster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FundCluster  $fundCluster
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fccode = FundCluster::find($id);
        return response()->json($fccode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FundCluster  $fundCluster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FundCluster $fundCluster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FundCluster  $fundCluster
     * @return \Illuminate\Http\Response
     */
    public function destroy(FundCluster $fundCluster)
    {
        //
    }

    public function deleteFC($id)
    {
        FundCluster::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
