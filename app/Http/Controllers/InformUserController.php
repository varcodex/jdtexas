<?php

namespace App\Http\Controllers;

use App\InformUsers;
use App\ResponsibilityCenter;
use App\TrackDocument;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Carbon;

class InformUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if($user=Sentinel::Check()){
        } else {
            return view('login');
        }
        $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();
        return view('informuser', compact('rcs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($userInfo=Sentinel::Check()){
        }
        $data = request()->validate([
            'to_rc_id' => 'required|numeric',
            'smessage' => 'required|max:40',
        ]);
        if(InformUsers::where('message', '=', Input::get('smessage'))->exists()){
            return response()->json(['code'=>'1']);
        }else{
            InformUsers::updateOrCreate(['id' => $request->id],
                            ['from_rc_id' => $userInfo->rc_id, 
                            'to_rc_id' => $request->to_rc_id,
                            'message' => $request->smessage]);
        }
         return redirect('informuser')->with('success', 'Message Sent Successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lesson = InformUsers::find($id);
        $lesson->is_read = now();
        $lesson->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
