<?php

namespace App\Http\Controllers;

use App\DocumentLog;
use Illuminate\Http\Request;
use DB;
class DocumentLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $barcode = $request->id;
        $last_row=DB::table('document_logs')->orderBy('created_at', 'desc')->first();
        $datas = DB::table('document_logs')
            ->where('tracking_no', $request->id)
            ->orderBy('created_at', 'desc')->get();
        return view('timeline', compact('datas', 'barcode','last_row'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DocumentLog  $documentLog
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentLog $documentLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DocumentLog  $documentLog
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentLog $documentLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DocumentLog  $documentLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DocumentLog $documentLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DocumentLog  $documentLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentLog $documentLog)
    {
        //
    }
}
