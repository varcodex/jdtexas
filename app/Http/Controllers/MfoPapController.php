<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\MfoPap;
use Session;
use DataTables;

class MfoPapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = MfoPap::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editMFOCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteMFOCode">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('mfo_paps.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'mfo_code' => 'required|min:3',
            'mfo_particulars' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(MfoPap::where('mfo_code', '=', Input::get('mfo_code'))->exists()){
                return response()->json(['code'=>'1']);
            }else{
                MfoPap::updateOrCreate(['id' => $request->id],
                                ['mfo_code' => $request->mfo_code, 'mfo_particulars' => $request->mfo_particulars,
                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(MfoPap::where([
                ['id', '=', Input::get('id')],
                ['mfo_code', '=', Input::get('mfo_code')],
                ['is_deleted', '<>', '1']
                ])->exists()){
                    MfoPap::find(Input::get('id'))
                                ->update(['mfo_code' => $request->mfo_code,
                                        'mfo_particulars' => $request->mfo_particulars,
                                        'is_deleted' => '0']);
            }else{
                if(MfoPap::where([
                                    ['is_deleted', '<>', '1'],
                                    ['mfo_code', '=', Input::get('mfo_code')],
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    MfoPap::find(Input::get('id'))
                                    ->update(['mfo_code' => $request->mfo_code,
                                            'mfo_particulars' => $request->mfo_particulars,
                                            'is_deleted' => '0']);
                }

            }
        }

        return response()->json(['success'=>'MFO/PAP Codes saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MfoPap  $mfoPap
     * @return \Illuminate\Http\Response
     */
    public function show(MfoPap $mfoPap)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MfoPap  $mfoPap
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mfocode = MfoPap::find($id);
        return response()->json($mfocode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MfoPap  $mfoPap
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MfoPap $mfoPap)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MfoPap  $mfoPap
     * @return \Illuminate\Http\Response
     */
    public function destroy(MfoPap $mfoPap)
    {
        //
    }

    public function deleteMP($id)
    {
        MfoPap::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
