<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactFormController extends Controller
{
    public function create(){
        return view('contact');
    }

    public function store(Request $request){
        $data = request()->validate([
            'name' => 'required',
            'email' => 'required | email',
            'content' => 'required',
        ]);
        //send an email
        Mail::send(new ContactMail($request));
      
         return redirect('contact')->with('success', 'Thanks for your email. We\'ll be in touch!');
    }
}
