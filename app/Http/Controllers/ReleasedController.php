<?php

namespace App\Http\Controllers;

use App\DocumentLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;
use App\DocumentTypes;
use App\ResponsibilityCenter;
use App\Payee;
use App\TrackDocument;
use App\Purposes;


class ReleasedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            //dd('this is the Released');
            if($userInfo=Sentinel::Check()){
            } else {
                return view('login');
            }
            $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('signatory_name', 'ASC')->get();  
            $purposes = Purposes::select('id','purpose')->where('is_deleted', '<>', '1')->orderBy('purpose', 'ASC')->get();
            $tracks = TrackDocument::select('id','tracking_no')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get(); 
            $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();
    
            //filter display document types based from the forms used in the office 
            $doctypes = DocumentTypes::select('id','particulars')
                ->where('rc_id', $userInfo->rc_id)
                ->orWhere('rc_id', 0)
                ->where('is_deleted', '<>', '1')
                ->orderBy('particulars', 'ASC')->get();
    
            //get the track_documents data for documents with status of In Process
            $data = DB::table('track_documents')
                    ->join('doc_type', 'doc_type.id', '=', 'track_documents.type')
                    ->join('purposes','purposes.id','=','track_documents.purpose_id')
                    ->leftJoin('payees','payees.id','=','track_documents.payee_id')
                    ->select('track_documents.id as id', 'track_documents.tracking_no as tracking_no', 'track_documents.title as title',
                            'track_documents.office_action as office_action', 'payees.signatory_name as signatory_name',
                            'doc_type.id as type_id','doc_type.particulars as particulars','purposes.purpose as purpose', 
                            'track_documents.type as type', 'track_documents.remarks as remarks','track_documents.track_status as track_status',
                            'track_documents.created_at as created_at', 'track_documents.updated_at as updated_at')
                    ->where('track_documents.current_office', $userInfo->rc_id)
                    ->whereIn('track_documents.track_status',['On Route'])
                    ->whereIn('track_documents.office_action',['Released'])
                    ->where('track_documents.is_deleted', '<>', '1')
                    ->orderBy('doc_type.id', 'ASC')
                    ->get();
    
                     if (request()->ajax()){   
                        return DataTables::of($data)
                            //->editColumn('created_at',function(TrackDocument $data) {
                           // return $data->created_at->diffForHumans();
                           // })
                            ->addIndexColumn()
                            ->addColumn('action', function($row){

                                    $btn = ' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm tagTerminal">File a Copy</a>';
                                   return $btn;
                            })
                            ->rawColumns(['action'])
                            ->make(true);
                        }
            return view('trackdocument.released', compact('doctypes','rcs','payees','purposes','tracks'));    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title' => 'required|min:3',
            'type_id' => 'required',
            'purpose_id' => 'required',
        ]);
    
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }
    
            if($userInfo=Sentinel::Check()){
    
            }
            //create a tracking number automatically based from rc, month, day, get the series based on RC month and day
            //use 1 if not yet existing
            if($request->status == "finalize"){
                    if(TrackDocument::where([
                                    ['id', '=', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        TrackDocument::find(Input::get('id'))
                                                    ->update(['track_status' => "On Route",
                                                    'office_action' => "Released"]);
    
                                        DocumentLog::create(['tracking_no' => $request->tracking_no,
                                                    'title' => $request->title,
                                                    'type' => $request->type_id,
                                                    'purpose_id' => $request->purpose_id,
                                                    'remarks' => $request->remarks,
                                                    'track_status' => "On Route",
                                                    'office_action' => "Released",
                                                    'orig_office' => $userInfo->rc_id,
                                                    'current_office' => $userInfo->rc_id,
                                                    'payee_id' =>$request->payee_id,
                                                    'user_id' =>$userInfo->id,
                                                    'created_at' => now() ]);  
    
                    }else{
                        if(TrackDocument::where([
                                            ['id', '=', Input::get('id')],
                                            ['is_deleted', '<>', '1']
                                            ])->exists()){
                                                return response()->json(['code'=>'1']);
                        }else{
                            TrackDocument::find(Input::get('id'))
                                            ->update([  'track_status' => "On Route",
                                                        'office_action' => "Released"]);     
    
                            DocumentLog::create(['tracking_no' => $request->tracking_no,
                                                        'title' => $request->title,
                                                        'type' => $request->type_id,
                                                        'purpose_id' => $request->purpose_id,
                                                        'remarks' => $request->remarks,
                                                        'track_status' => "On Route",
                                                        'office_action' => "Released",
                                                        'orig_office' => $userInfo->rc_id,
                                                        'current_office' => $userInfo->rc_id,
                                                        'payee_id' =>$request->payee_id,
                                                        'user_id' =>$userInfo->id,
                                                        'created_at' => now()]); 
                        }            
                    }  

                }elseif($request->status == "terminal"){
                    if(TrackDocument::where([
                            ['id', '=', Input::get('id')],
                            ['is_deleted', '<>', '1']
                             ])->exists()){
                            TrackDocument::find(Input::get('id'))
                                        ->update(['track_status' => "Terminal",
                                        'office_action' => "Filed"]);

                            DocumentLog::create(['tracking_no' => $request->tracking_no,
                                        'title' => $request->title,
                                        'type' => $request->type_id,
                                        'purpose_id' => $request->purpose_id,
                                        'remarks' => $request->remarks,
                                        'track_status' => "Terminal",
                                        'office_action' => "Filed",
                                        'orig_office' => $userInfo->rc_id,
                                        'current_office' => $userInfo->rc_id,
                                        'payee_id' =>$request->payee_id,
                                        'user_id' =>$userInfo->id,
                                        'created_at' => now() ]);  

                    }else{
                        if(TrackDocument::where([
                                            ['id', '=', Input::get('id')],
                                            ['is_deleted', '<>', '1']
                                            ])->exists()){
                                                return response()->json(['code'=>'1']);
                        }else{
                            TrackDocument::find(Input::get('id'))
                                            ->update([  'track_status' => "On Route",
                                                        'office_action' => "Released"]);     

                            DocumentLog::create(['tracking_no' => $request->tracking_no,
                                                        'title' => $request->title,
                                                        'type' => $request->type_id,
                                                        'purpose_id' => $request->purpose_id,
                                                        'remarks' => $request->remarks,
                                                        'track_status' => "Terminal",
                                                        'office_action' => "Filed",
                                                        'orig_office' => $userInfo->rc_id,
                                                        'current_office' => $userInfo->rc_id,
                                                        'payee_id' =>$request->payee_id,
                                                        'user_id' =>$userInfo->id,
                                                        'created_at' => now()]); 
                        }
    
                    }  
                }

            return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dtcode = TrackDocument::find($id);
        return response()->json($dtcode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
