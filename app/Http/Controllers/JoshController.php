<?php namespace App\Http\Controllers;

use App\User;
use App\ResponsibilityCenter;
use App\InformUsers;
use App\DocumentLog;
use App\TrackDocument;
use Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use View;
use Illuminate\Http\Request;
use DataTables;


class JoshController extends Controller {

	protected $countries = array(
			""   => "Select Country",
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia and Herzegowina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CD" => "Congo, the Democratic Republic of the",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"CI" => "Cote d'Ivoire",
			"HR" => "Croatia (Hrvatska)",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands (Malvinas)",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GN" => "Guinea",
			"GW" => "Guinea-Bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard and Mc Donald Islands",
			"VA" => "Holy See (Vatican City State)",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (Islamic Republic of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea, Democratic People's Republic of",
			"KR" => "Korea, Republic of",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libyan Arab Jamahiriya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau",
			"MK" => "Macedonia, The Former Yugoslav Republic of",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia, Federated States of",
			"MD" => "Moldova, Republic of",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"AN" => "Netherlands Antilles",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RE" => "Reunion",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"KN" => "Saint Kitts and Nevis",
			"LC" => "Saint LUCIA",
			"VC" => "Saint Vincent and the Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome and Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia (Slovak Republic)",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia and the South Sandwich Islands",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SH" => "St. Helena",
			"PM" => "St. Pierre and Miquelon",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard and Jan Mayen Islands",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan, Province of China",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic of",
			"TH" => "Thailand",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad and Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks and Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"GB" => "United Kingdom",
			"US" => "United States",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VE" => "Venezuela",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis and Futuna Islands",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe"
	);
	/**
	 * Message bag.
	 *
	 * @var Illuminate\Support\MessageBag
	 */
	protected $messageBag = null;

    /**
     * Initializer.
     *
     */
	public function __construct()
	{
		$this->messageBag = new MessageBag;

	}

	/**
	* Crop Demo
	*/
	public function crop_demo()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$targ_w = $targ_h = 150;
			$jpeg_quality = 99;

			$src = base_path().'/public/assets/img/cropping-image.jpg';

            $img_r = imagecreatefromjpeg($src);

			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

			imagecopyresampled($dst_r,$img_r,0,0,intval($_POST['x']),intval($_POST['y']), $targ_w,$targ_h, intval($_POST['w']),intval($_POST['h']));

			header('Content-type: image/jpeg');
			imagejpeg($dst_r,null,$jpeg_quality);

			exit;
		}
	}

    public function showHome()
    {
		if($userInfo = Sentinel::check()){   
			//this will display all documents that originated in the current office
			//that is not yet marked as Filed
			$data = DB::table('document_logs')
			->join('doc_type', 'doc_type.id', '=', 'document_logs.type')
			->join('responsibility_centers as a','a.id','=','document_logs.current_office')
			->join('responsibility_centers as b','b.id','=','document_logs.orig_office')
			->select('document_logs.id as id', 'document_logs.tracking_no as barcode_id', 
					'document_logs.title as description', 'document_logs.track_status as track_status', 
					'doc_type.particulars as particulars', 'document_logs.office_action as office_action', 
					'document_logs.remarks as remarks', 
					'a.rc_description as current_office','b.rc_description as orig_office',
					'document_logs.created_at as created_at', 'document_logs.updated_at as updated_at')
			->where('document_logs.orig_office', $userInfo->rc_id)
			->orWhere('document_logs.current_office', $userInfo->rc_id)
			->whereDate('document_logs.created_at','=', Carbon::today()->toDateString())
			->orderBy('document_logs.created_at', 'desc')
			->get();

			//dd($data);
			 if (request()->ajax()){   
				return DataTables::of($data)
				->make(true);
			}
			
			return view('index');
		}
		else
			return view('login')->with('error', 'You must be logged in!');
    }

	public function showStore(Request $request){
		$validator = \Validator::make($request->all(), [
			'tracking_no_rcv' => ''
		]);

			if ($validator->fails())
			{
				return response()->json(['errors'=>$validator->errors()->all()]);
			}

			if($userInfo=Sentinel::Check()){

			}

			if($request->status_rcv == "receive"){
					if(TrackDocument::where([
							['tracking_no', '=', $request->tracking_no_rcv],
							['is_deleted', '<>', '1']
							])->exists())
							{
								$tracks = TrackDocument::where('tracking_no', $request->tracking_no_rcv)
										->where('is_deleted', '<>', '1')->get();
																
								foreach ($tracks as $tract) {
										$id = $tract->id;
										$tracking_no = $tract->tracking_no;
										$type = $tract->type;
										$title = $tract->title;
										$purpose_id = $tract->purpose_id;
										$remarks = $tract->remarks;
										$orig_office = $tract->orig_office;
										$current_office = $userInfo->rc_id;
										$payee_id = $tract->payee_id;
										$user_id = $userInfo->id;
								}

								TrackDocument::find($id)
											->update(['track_status' => "In Process",
											'office_action' => "Received",
											'current_office' => $current_office ]);    

								DocumentLog::create(['tracking_no' => $tracking_no,
										'title' => $title,
										'type' => $type,
										'purpose_id' => $purpose_id,
										'remarks' => $remarks,
										'track_status' => "In Process",
										'office_action' => "Received",
										'orig_office' => $orig_office,
										'current_office' => $current_office,
										'payee_id' =>$payee_id,
										'user_id' =>$user_id,
										'created_at' => now() ]);  
							}else{
								return response()->json(['code'=>'1']);
							}

			}elseif($request->status_tag == "terminal"){
				if(TrackDocument::where([
					['tracking_no', '=', $request->tracking_no_tag],
					['is_deleted', '<>', '1']
					])->exists())
					{
						$tracks = TrackDocument::where('tracking_no', $request->tracking_no_tag)
								->where('is_deleted', '<>', '1')->get();
														
						foreach ($tracks as $tract) {
								$id = $tract->id;
								$tracking_no = $tract->tracking_no;
								$type = $tract->type;
								$title = $tract->title;
								$purpose_id = $tract->purpose_id;
								$remarks = $tract->remarks;
								$orig_office = $tract->orig_office;
								$current_office = $userInfo->rc_id;
								$payee_id = $tract->payee_id;
								$user_id = $userInfo->id;
						}

						TrackDocument::find($id)
									->update(['track_status' => "Terminal",
									'office_action' => "Filed",
									'current_office' => $current_office]);    

						DocumentLog::create(['tracking_no' => $tracking_no,
								'title' => $title,
								'type' => $type,
								'purpose_id' => $purpose_id,
								'remarks' => $remarks,
								'track_status' => "Terminal",
								'office_action' => "Filed",
								'orig_office' => $orig_office,
								'current_office' => $current_office,
								'payee_id' =>$payee_id,
								'user_id' =>$user_id,
								'created_at' => now() ]);  
					}else{
						return response()->json(['code'=>'1']);
					}
			}				
		return response()->json(['success'=>$request->status_rcv]);

	}

    public function showView($name=null)
    {

    	if(View::exists($name))
		{
			if(Sentinel::check())
				return view($name);
			else
				return redirect('signin')->with('error', 'You must be logged in!');
		}
		else
		{
            abort('404');
		}
    }

    public function ActivityLog()
    {
        $activities = Activity::all();

        // Show the page
        return view('admin.activity_log', compact('activities'));
	}
	
}
