<?php

namespace App\Http\Controllers;
use PDF;
use App\AccountCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;

class AccountCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('account_codes')
                ->join('parent_account_codes', 'parent_account_codes.id', '=', 'account_codes.pac_id')
                ->join('allotment_classes', 'allotment_classes.id', '=', 'account_codes.ac_id')
                ->select('account_codes.id as id', 'account_codes.ac_code as ac_code', 'account_codes.ac_id as ac_id', 'account_codes.ac_description as ac_description',
                        'account_codes.approved_budget as approved_budget', 'account_codes.budget_adjustment as budget_adjustment',
                        'allotment_classes.uacs_particulars as uacs_particulars','account_codes.budget_total as budget_total', 'parent_account_codes.pac_description as pac_description', 'account_codes.created_at as created_at', 'account_codes.updated_at as updated_at')
                ->where('account_codes.is_deleted', '<>', '1')->orderBy('account_codes.id', 'ASC')->get();

        $pacs = DB::table('parent_account_codes')
                    ->select('id', 'pac_description')
                    ->where('is_deleted', '<>', '1')->orderBy('pac_description', 'ASC')->get();

        $acs = DB::table('allotment_classes')
                    ->select('id', 'uacs_particulars')
                    ->where('is_deleted', '<>', '1')->orderBy('uacs_particulars', 'ASC')->get();
           

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editAC">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteAC">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('ac.child.index', compact('pacs','acs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'pac_code' => 'required',
            'ac_code' => 'required|min:3',
            'ac_description' => 'required',
            'approved_budget' => 'required|numeric',
            'budget_adjustment' => 'required|numeric',
            'budget_total' => 'required|numeric',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(AccountCode::where([
                                    ['ac_code', '=', $request->ac_code],
                                    ['pac_id', '=', $request->pac_code],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    AccountCode::updateOrCreate(['id' => $request->id],
                                                ['pac_id' => $request->pac_code,
                                                'ac_code' => $request->ac_code,
                                                'ac_id' => $request->ac_id,
                                                'ac_description' => $request->ac_description,
                                                'approved_budget' => $request->approved_budget,
                                                'budget_adjustment' => $request->budget_adjustment,
                                                'budget_total' => $request->budget_total,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(AccountCode::where([
                                    ['id', '=', Input::get('id')],
                                    ['ac_code', '=', Input::get('ac_code')],
                                    ['ac_id', '=', Input::get('ac_id')],
                                    ['pac_id', '=', Input::get('pac_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        AccountCode::find(Input::get('id'))
                                                    ->update(['ac_description' => $request->ac_description,
                                                            'ac_code' => $request->ac_code,
                                                            'pac_id' => $request->pac_code,
                                                            'ac_id' => $request->ac_id,
                                                            'approved_budget' => $request->approved_budget,
                                                            'budget_adjustment' => $request->budget_adjustment,
                                                            'budget_total' => $request->budget_total,
                                                            'is_deleted' => '0']);
                }else{
                    if(AccountCode::where([
                                        ['id', '=', Input::get('id')],
                                        ['ac_code', '=', Input::get('ac_code')],
                                        ['pac_id', '=', Input::get('pac_code')],
                                        ['ac_id', '=', Input::get('ac_id')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        AccountCode::find(Input::get('id'))
                                        ->update(['ac_code' => $request->ac_code,
                                                'ac_description' => $request->ac_description,
                                                'pac_id' => $request->pac_code,
                                                'ac_id' => $request->ac_id,
                                                'approved_budget' => $request->approved_budget,
                                                'budget_adjustment' => $request->budget_adjustment,
                                                'budget_total' => $request->budget_total,
                                                'is_deleted' => '0']);
                    }

                }
            }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AccountCode  $accountCode
     * @return \Illuminate\Http\Response
     */
    public function show(AccountCode $accountCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AccountCode  $accountCode
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('account_codes')
                ->join('parent_account_codes', 'parent_account_codes.id', '=', 'account_codes.pac_id')
                ->join('allotment_classes', 'allotment_classes.id', '=', 'account_codes.ac_id')
                ->select('account_codes.id as id', 'account_codes.ac_code as ac_code', 'account_codes.ac_id as ac_id','account_codes.ac_description as ac_description',
                        'account_codes.approved_budget as approved_budget', 'account_codes.budget_adjustment as budget_adjustment', 'account_codes.budget_total as budget_total',
                        'allotment_classes.uacs_particulars as uacs_particulars','parent_account_codes.id as pac_id', 'parent_account_codes.pac_code as pac_code', 'account_codes.created_at as created_at', 'account_codes.updated_at as updated_at')
                ->where('account_codes.id', '=', $id)->where('account_codes.is_deleted', '<>', '1')->orderBy('account_codes.id', 'ASC')->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AccountCode  $accountCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AccountCode $accountCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AccountCode  $accountCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(AccountCode $accountCode)
    {
        //
    }

    public function deleteACCode($id)
    {
        AccountCode::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }

    public function printPDF()
    {
         // This  $data array will be passed to our PDF blade
            $parentCategories = AccountCode::where('ac_id', NULL)
                ->where('is_deleted', '<>', '1')
                ->orderBy('account_codes.ac_id', 'ASC')->orderBy('account_codes.id', 'ASC')->get();
           
            $subTotals = DB::table('account_codes')
                ->selectRaw('ac_id, sum(approved_budget) as approved_budgetT, 
                    sum(budget_adjustment) as budget_adjustmentT, sum(budget_total) as budget_totalT')
                ->where('ac_id','<>', NULL)
                ->where('is_deleted', '<>', '1')
                ->groupBy('ac_id')
                ->orderBy('ac_id','ASC')->get();

            $grandTotals = DB::table('account_codes')
                ->selectRaw('sum(approved_budget) as approved_budgetGT, 
                    sum(budget_adjustment) as budget_adjustmentGT, sum(budget_total) as budget_totalGT')
                ->where('ac_id','<>', NULL)
                ->where('is_deleted', '<>', '1')
                ->orderBy('ac_id','ASC')->get();
            
            $pdf = PDF::loadView('reports.pdf_view', compact('parentCategories','subTotals','grandTotals'));  
            return $pdf->download('allotments.pdf'); 
    
    }

    public function printUtilizationPDF()
    {
            // This  $data array will be passed to our PDF blade
            //create a temporary table to hold mfo-pap colums
            //insert aggregate total for each mfo
            //print 
            $tempUtil = DB::statement("CREATE TABLE student_details(
                id int NOT NULL AUTO_INCREMENT,
                first_name varchar(50),
                last_name varchar(50),
                city_name varchar(50),
                email varchar(50),
                PRIMARY KEY (id))
            ");

            $parentCategories = AccountCode::where('ac_id', NULL)
                ->where('is_deleted', '<>', '1')
                ->orderBy('account_codes.ac_id', 'ASC')->orderBy('account_codes.id', 'ASC')->get();
           
            $subTotals = DB::table('account_codes')
                ->selectRaw('ac_id, sum(approved_budget) as approved_budgetT, 
                    sum(budget_adjustment) as budget_adjustmentT, sum(budget_total) as budget_totalT')
                ->where('ac_id','<>', NULL)
                ->where('is_deleted', '<>', '1')
                ->groupBy('ac_id')
                ->orderBy('ac_id','ASC')->get();

            $grandTotals = DB::table('account_codes')
                ->selectRaw('sum(approved_budget) as approved_budgetGT, 
                    sum(budget_adjustment) as budget_adjustmentGT, sum(budget_total) as budget_totalGT')
                ->where('ac_id','<>', NULL)
                ->where('is_deleted', '<>', '1')
                ->orderBy('ac_id','ASC')->get();
           
            $pdf = PDF::loadView('reports.pdf_view_util', compact('parentCategories','subTotals','grandTotals'));  
            return $pdf->download('utilization.pdf'); 
    
    }


}
