<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showHome()
    {
        return view('home');
    }

    public function showContactUs()
    {
        return view('contact');
    }

      public function showAboutUs()
    {
        return view('aboutus');
    }
    
    public function showTimeline()
    {
        return view('timeline');
    }
}
