<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use DataTables;
use App\DocumentTypes;
use App\ResponsibilityCenter;
use App\Payee;
use App\TrackDocument;
use App\Purposes;
use PDF;

class TrackDocumentController extends Controller
{
    public function onroute(){
        //dd('this is the OnRoute');
        if($userInfo=Sentinel::Check()){
        } else {
            return view('login');
        }
        $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('signatory_name', 'ASC')->get();  
        $purposes = Purposes::select('id','purpose')->where('is_deleted', '<>', '1')->orderBy('purpose', 'ASC')->get();
        $tracks = TrackDocument::select('id','tracking_no')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get(); 
        $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();

        //filter display document types based from the forms used in the office 
        $doctypes = DocumentTypes::select('id','particulars')
            ->where('rc_id', $userInfo->rc_id)
            ->orWhere('rc_id', 0)
            ->where('is_deleted', '<>', '1')
            ->orderBy('particulars', 'ASC')->get();

        //get the track_documents data for documents with status of In Process
        $data = DB::table('track_documents')
                ->join('doc_type', 'doc_type.id', '=', 'track_documents.type')
                ->join('purposes','purposes.id','=','track_documents.purpose_id')
                ->leftJoin('payees','payees.id','=','track_documents.payee_id')
                ->select('track_documents.id as id', 'track_documents.tracking_no as tracking_no', 'track_documents.title as title', 'track_documents.office_action as office_action', 
                        'payees.signatory_name as signatory_name','doc_type.id as type_id','doc_type.particulars as particulars',
                        'purposes.purpose as purpose', 'track_documents.type as type', 'track_documents.remarks as remarks','track_documents.track_status as track_status',
                        'track_documents.created_at as created_at', 'track_documents.updated_at as updated_at')
                ->where('track_documents.orig_office',$userInfo->rc_id)
                ->where('track_documents.current_office',$userInfo->rc_id)
                ->where('track_documents.track_status','On Route')
                ->where('track_documents.is_deleted', '<>', '1')
                ->orderBy('doc_type.id', 'ASC')
                ->get();

                 if (request()->ajax()){   
                    return DataTables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                           
                               //$btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDTCode">Edit</a>';
                              // $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDoc">Delete</a>';
                               $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Finalize" class="btn btn-warning btn-sm releaseDTCode">Release</a>';
                               return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                    }
        return view('trackdocument.onroute', compact('doctypes','rcs','payees','purposes','tracks'));

    }

    public function vfiled(Request $request){ 
        if($userInfo=Sentinel::Check()){
        } else {
            return view('login');
        }

        if(request()->ajax()){
            if($request->office_action){
                $data = DB::table('document_logs')
                        ->leftJoin('doc_type', 'doc_type.id', '=', 'document_logs.type')
                        ->leftJoin('purposes','purposes.id','=','document_logs.purpose_id')
                        ->leftJoin('payees','payees.id','=','document_logs.payee_id')
                        ->select('document_logs.id as id', 'document_logs.tracking_no as tracking_no', 
                                'document_logs.title as title', 'payees.signatory_name as signatory_name',
                                'doc_type.id as type_id','doc_type.particulars as particulars',
                                'purposes.purpose as purpose', 'document_logs.office_action as office_action',
                                'document_logs.type as type', 'document_logs.remarks as remarks',
                                'document_logs.track_status as track_status',
                                'document_logs.created_at as created_at','document_logs.updated_at as updated_at')
                        ->where('document_logs.current_office', $userInfo->rc_id)
                        ->where('document_logs.office_action', $request->office_action)
                        ->orderBy('document_logs.created_at', 'ASC')
                        ->get();
            }else {
                $data = DB::table('document_logs')
                ->leftJoin('doc_type', 'doc_type.id', '=', 'document_logs.type')
                ->leftJoin('purposes','purposes.id','=','document_logs.purpose_id')
                ->leftJoin('payees','payees.id','=','document_logs.payee_id')
                ->select('document_logs.id as id', 
                                'document_logs.tracking_no as tracking_no', 
                                'document_logs.title as title', 'payees.signatory_name as signatory_name',
                                'doc_type.id as type_id','doc_type.particulars as particulars',
                                'purposes.purpose as purpose', 'document_logs.office_action as office_action',
                                'document_logs.type as type', 'document_logs.remarks as remarks',
                                'document_logs.track_status as track_status',
                                'document_logs.created_at as created_at','document_logs.updated_at as updated_at')
                ->where('document_logs.current_office', $userInfo->rc_id)
                ->orderBy('document_logs.created_at', 'ASC')
                ->get();
            }

            return DataTables::of($data)->make(true);
        }

        return view('trackdocument.vfiled');

    }



}
