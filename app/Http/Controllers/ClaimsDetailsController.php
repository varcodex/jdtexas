<?php

namespace App\Http\Controllers;

use App\ClaimsDetails;
use Illuminate\Http\Request;

class ClaimsDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClaimsDetails  $claimsDetails
     * @return \Illuminate\Http\Response
     */
    public function show(ClaimsDetails $claimsDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClaimsDetails  $claimsDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(ClaimsDetails $claimsDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClaimsDetails  $claimsDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClaimsDetails $claimsDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClaimsDetails  $claimsDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClaimsDetails $claimsDetails)
    {
        //
    }
}
