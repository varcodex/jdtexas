<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\ParentResponsibilityCenter;
use Session;
use DataTables;


class PRCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
        *$prcs = ParentResponsibilityCenter::all();
        *return view('rc.parent.index', compact('prcs'));
        */
        $data = ParentResponsibilityCenter::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editPRCCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deletePRCCode">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('rc.parent.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'prc_code' => 'required|min:3',
            'prc_description' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(ParentResponsibilityCenter::where('prc_code', '=', Input::get('prc_code'))->exists()){
                return response()->json(['code'=>'1']);
            }else{
                ParentResponsibilityCenter::updateOrCreate(['id' => $request->id],
                                                            ['prc_code' => $request->prc_code,
                                                            'prc_description' => $request->prc_description,
                                                            'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(ParentResponsibilityCenter::where([
                ['prc_code', '=', Input::get('prc_code')],
                ['id', '=', Input::get('id')],
                ['is_deleted', '<>', '1']
                ])->exists()){
                    ParentResponsibilityCenter::find(Input::get('id'))
                            ->update(['prc_description' => $request->prc_description,
                                    'prc_code' => $request->prc_code,
                                    'is_deleted' => '0']);
            }else{
                if(ParentResponsibilityCenter::where([
                                    ['prc_code', '=', Input::get('prc_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['prc_code'=>'1']);
                }else{
                    ParentResponsibilityCenter::find(Input::get('id'))->update(
                                    ['prc_description' => $request->prc_description,
                                    'prc_code' => $request->prc_code,
                                    'is_deleted' => '0']);
                }

            }
        }

        return response()->json(['success'=>'Parent Responsibility Center saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prccode = ParentResponsibilityCenter::find($id);
        return response()->json($prccode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deletePRC($id)
    {
        ParentResponsibilityCenter::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }

}
