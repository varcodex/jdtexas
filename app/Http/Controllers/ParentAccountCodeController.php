<?php

Namespace App\Http\Controllers;

use App\ParentAccountCode;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;


class ParentAccountCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $data = ParentAccountCode::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        $data = DB::table('parent_account_codes')
                ->join('allotment_classes', 'allotment_classes.id', '=', 'parent_account_codes.ac_id')
                ->select('parent_account_codes.id as id', 'parent_account_codes.pac_code as pac_code', 'parent_account_codes.pac_description as pac_description',
                        'allotment_classes.uacs_accronym as uacs_accronym', 'parent_account_codes.created_at as created_at', 'parent_account_codes.updated_at as updated_at')
                ->where('parent_account_codes.is_deleted', '<>', '1')->orderBy('parent_account_codes.id', 'ASC')->get();

        $acs = DB::table('allotment_classes')
        ->select('id', 'uacs_accronym')
        ->where('is_deleted', '<>', '1')->orderBy('uacs_accronym', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editPACCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deletePACCode">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }



        return view('ac.parent.index', compact('acs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'pac_code' => 'required|min:3',
            'pac_description' => 'required|min:3',
            'ac_id' => 'required',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(ParentAccountCode::where([
                                ['pac_code', '=', Input::get('pac_code')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    return response()->json(['pac_code'=>'1']);
            }else{
                ParentAccountCode::updateOrCreate(['id' => $request->id],
                                                            ['pac_code' => $request->pac_code,
                                                                'pac_description' => $request->pac_description,
                                                                'ac_id' => $request->ac_id,
                                                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){

            if(ParentAccountCode::where([
                                ['pac_code', '=', Input::get('pac_code')],
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    ParentAccountCode::find(Input::get('id'))
                                            ->update(['pac_description' => $request->pac_description,
                                                    'ac_id' => $request->ac_id,
                                                    'is_deleted' => '0']);
            }else{
                if(ParentAccountCode::where([
                                    ['pac_code', '=', Input::get('pac_code')],
                                    ['id', '<>', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['pac_code'=>'1']);
                }else{
                    ParentAccountCode::find(Input::get('id'))->update(
                        ['pac_code' => $request->pac_code,
                            'pac_description' => $request->pac_description,
                            'ac_id' => $request->ac_id,
                            'is_deleted' => '0']);
                }

            }
        }

        return response()->json(['success'=>$request->status]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ParentAccountCode  $ParentAccountCode
     * @return \Illuminate\Http\Response
     */
    public function show(ParentAccountCode $ParentAccountCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ParentAccountCode  $ParentAccountCode
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('parent_account_codes')
                ->join('allotment_classes', 'allotment_classes.id', '=', 'parent_account_codes.ac_id')
                ->select('parent_account_codes.id as id', 'parent_account_codes.pac_code as pac_code', 'parent_account_codes.pac_description as pac_description',
                        'allotment_classes.id as ac_id', 'parent_account_codes.created_at as created_at', 'parent_account_codes.updated_at as updated_at')
                        ->where('parent_account_codes.id', '=', $id)->where('parent_account_codes.is_deleted', '<>', '1')->orderBy('parent_account_codes.id', 'ASC')->get();
        return response()->json($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ParentAccountCode  $ParentAccountCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ParentAccountCode $ParentAccountCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ParentAccountCode  $ParentAccountCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(ParentAccountCode $ParentAccountCode)
    {
        //
    }

    public function deletePAC($id)
    {
        ParentAccountCode::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }

}
