<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;
use App\Purposes;
use App\DocumentTypes;
use App\ResponsibilityCenter;

class DocumentTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();
        $purposes = Purposes::select('id','purpose')->where('is_deleted', '<>', '1')->orderBy('purpose', 'ASC')->get();
        $data =  DB::table('doc_type')
            ->leftJoin('responsibility_centers', 'responsibility_centers.id', '=', 'doc_type.rc_id')
            ->leftJoin('purposes','purposes.id','doc_type.purpose_id')
            ->select('doc_type.id as id', 'doc_type.doc_code as doc_code','doc_type.particulars as particulars', 'doc_type.purpose_id', 'purposes.purpose as purpose',
                     'doc_type.category as category', 'responsibility_centers.rc_description as rc_description','doc_type.created_at as created_at',
                     'doc_type.updated_at as updated_at') 
            ->where('doc_type.is_deleted', '<>', '1')->orderBy('doc_type.id', 'ASC')->get();   
            //dd($data);
            if (request()->ajax()){
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDT">Edit</a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDTCode">Delete</a>';

                            return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            }

        return view('doctype.index',compact('rcs','purposes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'doc_code' => 'required',
            'particulars' => 'required|min:3',
            'purpose_id' => '',
            'category' => 'required',
            'rc_id' => '',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(DocumentTypes::where([
                                    ['doc_code', '=', $request->doc_code],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                    return response()->json(['code'=>'1']);
                }else{
                    DocumentTypes::updateOrCreate(['id' => $request->id],
                                                ['doc_code' => $request->doc_code,
                                                'particulars' => $request->particulars,
                                                'purpose_id' => $request->purpose_id,
                                                'category' => $request->category,
                                                'rc_id' => $request->rc_id,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(DocumentTypes::where([
                                    ['id', '=', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        DocumentTypes::find(Input::get('id'))
                                                    ->update(['doc_code' => $request->doc_code,
                                                            'particulars' => $request->particulars,
                                                            'purpose_id' => $request->purpose_id,
                                                            'category' => $request->category,
                                                            'rc_id' => $request->rc_id,
                                                            'is_deleted' => '0']);
                }else{
                    if(DocumentTypes::where([
                                        ['id', '=', Input::get('id')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        DocumentTypes::find(Input::get('id'))
                                        ->update(['doc_code' => $request->doc_code,
                                                'particulars' => $request->particulars,
                                                'purpose_id' => $request->purpose_id,
                                                'category' => $request->category,
                                                'rc_id' => $request->rc_id,
                                                'is_deleted' => '0']);
                    }
                    
                }
            }

        return response()->json(['success'=>$request->status]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dtcode = DocumentTypes::find($id);
        return response()->json($dtcode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function deleteDTCode($id)
    {
        DocumentTypes::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }


}
