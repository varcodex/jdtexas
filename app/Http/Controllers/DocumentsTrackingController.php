<?php

namespace App\Http\Controllers;

use App\DocumentLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Session;
use DataTables;
use Cron\MonthField;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyMail;

use App\DocumentTypes;
use App\ResponsibilityCenter;
use App\Payee;
use App\TrackDocument;
use App\Purposes;




//Tracking Status
/*  1 - In Process (editing is allowed)
    2 - On Route (document is finalized and being transfered to other offices)
    3 - Terminal (document has been filed in the receiving office - orgiginal/photo copy/duplicates)

//Office Actions
    1 - Processed (signed, approved, noted, recommended)
    2 - On hold (with missing entries/missing signature or problems in attachments)

//Remarks
    eg. Signed, Approved, Recommended, etc

*/ 
class DocumentsTrackingController extends Controller
{
    public function index(){
        //dd('this is the index');
        if($userInfo=Sentinel::Check()){
        } else {
            return view('login');
        }
        $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('signatory_name', 'ASC')->get();  
        $purposes = Purposes::select('id','purpose')->where('is_deleted', '<>', '1')->orderBy('purpose', 'ASC')->get();
        $tracks = TrackDocument::select('id','tracking_no')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get(); 
        $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();

        //filter display document types based from the forms used in the office 
        $doctypes = DocumentTypes::select('id','particulars')
            ->where('rc_id', $userInfo->rc_id)
            ->orWhere('rc_id', 0)
            ->where('is_deleted', '<>', '1')
            ->orderBy('particulars', 'ASC')->get();

        //get the track_documents data for documents with status of In Process
        $data = DB::table('track_documents')
                ->join('doc_type', 'doc_type.id', '=', 'track_documents.type')
                ->join('purposes','purposes.id','=','track_documents.purpose_id')
                ->leftJoin('payees','payees.id','=','track_documents.payee_id')
                ->select('track_documents.id as id', 'track_documents.tracking_no as tracking_no', 'track_documents.title as title', 
                        'payees.signatory_name as signatory_name','doc_type.id as type_id','doc_type.particulars as particulars','purposes.purpose as purpose', 'track_documents.type as type', 'track_documents.remarks as remarks','track_documents.track_status as track_status',
                        'track_documents.created_at as created_at', 'track_documents.updated_at as updated_at')
                ->where('track_documents.current_office',$userInfo->rc_id)
                ->where('track_documents.track_status','In Process')
                ->where('track_documents.is_deleted', '<>', '1')
                ->orderBy('doc_type.id', 'ASC')
                ->get();

                 if (request()->ajax()){   
                    return DataTables::of($data)
                        ->addIndexColumn()
                        ->addColumn('action', function($row){
                           
                               $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDTCode">Edit</a>';
                               $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDoc">Delete</a>';
                               $btn = $btn. '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Finalize" class="btn btn-warning btn-sm finalizeDTCode">Finalize</a>';
                               return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
                    }
        return view('documentstracking.index', compact('doctypes','rcs','payees','purposes','tracks'));

    }

/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    public function store(Request $request){
    
    $validator = \Validator::make($request->all(), [
        'title' => 'required|min:3',
        'type_id' => 'required',
        'purpose_id' => 'required',
    ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($userInfo=Sentinel::Check()){

        }
        //create a tracking number automatically based from rc, month, day, get the series based on RC month and day
        //use 1 if not yet existing
        if($request->status == "create"){
                //get the tracking number first
              
                $series=1;
        
                $lastYearMonth = TrackDocument::whereYear('created_at', '=', date('Y'))
                            ->whereMonth('created_at', '=', date('m'))
                            ->where('current_office',$userInfo->rc_id)->get();
                foreach ($lastYearMonth as $yrmo) {
                    if(! empty($yrmo))
                        $series = preg_replace('/[^0-9]/','', substr($yrmo->tracking_no,12)) + 1;
                    else
                        $series = 1;
                }
        
                $rcs = ResponsibilityCenter::select('id','rc_description')
                ->where('id', $userInfo->rc_id)
                ->where('is_deleted', '<>', '1')
                ->orderBy('rc_description', 'ASC')->get();
                
                foreach ($rcs as $rc) {
                    $tracking_no = $rc->rc_description.'-'.date("Y").'-'.date("m").'-'. $series;
                }
                //email if $request->payee_id is not empty
                if(! empty($request->payee_id) ){
                    //get email address of person concerned
                    $payees = Payee::where('id',$request->payee_id)->get();
                    foreach ($payees as $payee) {
                        $email = $payee->email_address; 
                        $name = $payee ->signatory_name;
                    }
                    //get the url of the DTExAS timeline page and add the trackingNumber
                    $trace_no = url('/'). '/timeline/trace?id='. $tracking_no;
                    Mail::send(new NotifyMail($name,$email,$trace_no));
                }

                //save here    
                if(TrackDocument::where([
                                    ['tracking_no', '=', $tracking_no],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                    return response()->json(['code'=>'1']);
                }else{
                    TrackDocument::updateOrCreate(['id' => $request->id],
                                                ['tracking_no' => $tracking_no,
                                            'title' => $request->title,
                                            'type' => $request->type_id,
                                            'purpose_id' => $request->purpose_id,
                                            'remarks' => $request->remarks,
                                            'track_status' => "In Process",
                                            'office_action' => "Registered",
                                            'orig_office' => $userInfo->rc_id,
                                            'current_office' => $userInfo->rc_id,
                                            'payee_id' =>$request->payee_id,
                                            'user_id' =>$userInfo->id, 
                                            'is_deleted' => '0']);

                    DocumentLog::create(['tracking_no' => $tracking_no,
                                            'title' => $request->title,
                                            'type' => $request->type_id,
                                            'purpose_id' => $request->purpose_id,
                                            'remarks' => $request->remarks,
                                            'track_status' => "In Process",
                                            'office_action' => "Registered",
                                            'orig_office' => $userInfo->rc_id,
                                            'current_office' => $userInfo->rc_id,
                                            'payee_id' =>$request->payee_id,
                                            'user_id' =>$userInfo->id]);    
                }
        }elseif($request->status == "edit"){
            if(TrackDocument::where([
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    TrackDocument::find(Input::get('id'))
                                                ->update(['title' => $request->title,
                                                'type' => $request->type_id,
                                                'purpose_id' => $request->purpose_id,
                                                'remarks' => $request->remarks,
                                                'track_status' => "In Process",
                                                'office_action' => "Registered",
                                                'orig_office' => $userInfo->rc_id,
                                                'current_office' => $userInfo->rc_id,
                                                'payee_id' =>$request->payee_id,
                                                'user_id' =>$userInfo->id,
                                                'is_deleted' => '0']);

            }else{
                if(TrackDocument::where([
                                    ['id', '=', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    TrackDocument::find(Input::get('id'))
                                    ->update(['title' => $request->title,
                                                'type' => $request->type_id,
                                                'purpose_id' => $request->purpose_id,
                                                'remarks' => $request->remarks,
                                                'track_status' => "In Process",
                                                'office_action' => "Registered",
                                                'orig_office' => $userInfo->rc_id,
                                                'current_office' => $userInfo->rc_id,
                                                'payee_id' =>$request->payee_id,
                                                'user_id' =>$userInfo->id,
                                                'is_deleted' => '0']);  
                }
                
            }

        }elseif($request->status == "finalize"){
                if(TrackDocument::where([
                                ['id', '=', Input::get('id')],
                                ['is_deleted', '<>', '1']
                                ])->exists()){
                                    TrackDocument::find(Input::get('id'))
                                                ->update(['track_status' => "On Route",
                                                'office_action' => "Released"]);

                                    DocumentLog::create(['tracking_no' => $request->tracking_no,
                                                'title' => $request->title,
                                                'type' => $request->type_id,
                                                'purpose_id' => $request->purpose_id,
                                                'remarks' => $request->remarks,
                                                'track_status' => "On Route",
                                                'office_action' => "Released",
                                                'orig_office' => $userInfo->rc_id,
                                                'current_office' => $userInfo->rc_id,
                                                'payee_id' =>$request->payee_id,
                                                'user_id' =>$userInfo->id,
                                                'created_at' => now() ]);  

                }else{
                    if(TrackDocument::where([
                                        ['id', '=', Input::get('id')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        TrackDocument::find(Input::get('id'))
                                        ->update([  'track_status' => "On Route",
                                                    'office_action' => "Released"]);     

                        DocumentLog::create(['tracking_no' => $request->tracking_no,
                                                    'title' => $request->title,
                                                    'type' => $request->type_id,
                                                    'purpose_id' => $request->purpose_id,
                                                    'remarks' => $request->remarks,
                                                    'track_status' => "On Route",
                                                    'office_action' => "Released",
                                                    'orig_office' => $userInfo->rc_id,
                                                    'current_office' => $userInfo->rc_id,
                                                    'payee_id' =>$request->payee_id,
                                                    'user_id' =>$userInfo->id,
                                                    'created_at' => now()]); 
                    }
                
                }
        }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $dtcode = TrackDocument::find($id);
        return response()->json($dtcode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deleteDoc($id)
    {
        TrackDocument::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
    

}
