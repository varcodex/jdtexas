<?php

namespace App\Http\Controllers;

use App\ClaimsHeader;
use App\ClaimsDetails;
use App\AccountCode;
use App\AllotmentClass;
use App\FundCluster;
use App\Payee;
use App\ResponsibilityCenter;
use App\MfoPap;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Session;
use DataTables;

class ClaimsHeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('claims_headers')
                ->join('fund_clusters', 'fund_clusters.id', '=', 'claims_headers.id')
                ->join('payees', 'payees.id', '=', 'claims_headers.payee_id')
                ->select('claims_headers.id as id', 'claims_headers.barcode as barcode', 'fund_clusters.fc_particulars as fc_particulars',
                        'payees.signatory_name as signatory_name', 'claims_headers.status as status',
                        'claims_headers.created_at as created_at', 'claims_headers.updated_at as updated_at')
                ->where('claims_headers.is_deleted', '<>', '1')->orderBy('claims_headers.id', 'ASC')->get();

          $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="/budget/'.$row->id.'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editACCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteACCode">Delete</a>';
                        if($row->status == "Approved"){
                            $btn = $btn.' <a href="/printBurs/'.$row->id.'" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Print" class="btn btn-success btn-sm printACCode">Print</a>';
                        }
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('budget.index', compact('payees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $acs = AllotmentClass::select('id','uacs_accronym')->where('is_deleted', '<>', '1')->orderBy('uacs_accronym', 'ASC')->get();
        $fcs = FundCluster::select('id','fc_particulars')->where('is_deleted', '<>', '1')->orderBy('fc_particulars', 'ASC')->get();
        $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('signatory_name', 'ASC')->get();
        $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();
        $mfos = MfoPap::select('id','mfo_particulars')->where('is_deleted', '<>', '1')->orderBy('mfo_particulars', 'ASC')->get();
        $uacs = AccountCode::select('id','ac_description')->where('is_deleted', '<>', '1')->orderBy('ac_description', 'ASC')->get();
        $totalamounts = AccountCode::select('id','budget_total')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->limit(1)->get();
        return view('budget.create', compact('acs', 'fcs', 'payees', 'rcs', 'mfos','uacs', 'totalamounts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->status == "create"){
            $validator = \Validator::make($request->all(), [
                'payee' => 'required',
                'office' => 'required',
                'address' => 'required',
                'sign1' => 'required',
                'sign2' => 'required',
                'ac' => 'required',
                'fc' => 'required',
            ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            $account = json_decode($request->account, true);
            $status = false;
            If($userInfo = Sentinel::check()){}
            while (!$status)
            {
                $acCode = AllotmentClass::where('id', '=', $request->ac)->pluck('uacs_code');
                $fcCode = FundCluster::where('id', '=', $request->fc)->pluck('fc_code');
                $fcID = (int)ClaimsHeader::where('fund_cluster_id', '=', $request->fc)->get()->count();
                $result = $fcID+1;
                if(((int)$request->month)<10){
                    $month = '0'.$request->month;
                }else{
                    $month = $request->month;
                }

                $barcode = $acCode[0].'-'.$fcCode[0].'-'.$request->year.'-'.$month.'-'.(string)$result;
                $ch = ClaimsHeader::Create(array('barcode' => $barcode,
                                        'allotment_id' => $request->ac,
                                        'fund_cluster_id' => $request->fc,
                                        'year' => $request->year,
                                        'month' => $request->month,
                                        'payee_id' => $request->payee,
                                        'office' => $request->office,
                                        'address' => $request->address,
                                        'signatory_A_id' => $request->sign1,
                                        'signatory_B_id' => $request->sign2,
                                        'user_id' => $userInfo->id,
                                        'status' => 'In Progress',
                                        'is_deleted' => '0'
                                    ));

                $status = true;

                for($i = 0; $i < count($account); $i++){
                    $rcID = ResponsibilityCenter::where('rc_description', '=', $account[$i]['rc'])->pluck('id');

                    $mfoID = MfoPap::where('mfo_particulars', '=', $account[$i]['mfo'])->pluck('id');
                    $uacID = AccountCode::where('ac_description', '=', $account[$i]['uac'])->pluck('id');
                    ClaimsDetails::Create(array(
                                                'claim_header_id' => $ch->id,
                                                'rc_id' => $rcID[0],
                                                'particulars' => $account[$i]['par'],
                                                'mfo_id' => $mfoID[0],
                                                'uacs_id' => $uacID[0],
                                                'amount' => $account[$i]['amount'],
                                                'is_deleted' => '0'
                                                ));

                }
                $status = true;

            }
        return response()->json(['success'=>$request->status]);
        }elseif($request->status == "edit"){

            $validator = \Validator::make($request->all(), [
                    'payee' => 'required',
                    'office' => 'required',
                    'address' => 'required',
                    'sign1' => 'required',
                    'sign2' => 'required',
            ]);
            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            $account = json_decode($request->account, true);
            $status = false;
            while (!$status)
            {
                ClaimsHeader::find($request->id)
                                ->update([
                                        'payee_id' => $request->payee,
                                        'office' => $request->office,
                                        'address' => $request->address,
                                        'signatory_A_id' => $request->sign1,
                                        'signatory_B_id' => $request->sign2]);

                $result = ClaimsDetails::where('claim_header_id', $request->id)->delete();
                if($result){
                    for($i = 0; $i < count($account); $i++){
                        $rcID = ResponsibilityCenter::where('rc_description', '=', $account[$i]['rc'])->pluck('id');

                        $mfoID = MfoPap::where('mfo_particulars', '=', $account[$i]['mfo'])->pluck('id');
                        $uacID = AccountCode::where('ac_description', '=', $account[$i]['uac'])->pluck('id');
                        ClaimsDetails::Create(array(
                                                    'claim_header_id' => $request->id,
                                                    'rc_id' => $rcID[0],
                                                    'particulars' => $account[$i]['par'],
                                                    'mfo_id' => $mfoID[0],
                                                    'uacs_id' => $uacID[0],
                                                    'amount' => $account[$i]['amount'],
                                                    'is_deleted' => '0'
                                                    ));
                    }
                }
                $status = true;
            }
            return response()->json(['success'=>$request->status]);
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClaimsHeader  $claimsHeader
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ch = DB::table('claims_headers')
                ->select('claims_headers.id as id', 'claims_headers.payee_id as payee_id', 'claims_headers.office as office',
                        'claims_headers.address as address', 'claims_headers.signatory_A_id as signatory_A_id', 'claims_headers.signatory_B_id as signatory_B_id')
                ->where('claims_headers.id', '=', $id)->get();
        $chdetails = DB::table('claims_details')
                    ->join('responsibility_centers', 'responsibility_centers.id', '=', 'claims_details.rc_id')
                    ->join('mfo_paps', 'mfo_paps.id', '=', 'claims_details.mfo_id')
                    ->join('account_codes', 'account_codes.id', '=', 'claims_details.uacs_id')
                    ->select('responsibility_centers.rc_description as rc_description', 'claims_details.particulars as particulars',
                    'mfo_paps.mfo_particulars as mfo_particulars', 'account_codes.ac_description as ac_description', 'claims_details.amount as amount')
                    ->where('claims_details.claim_header_id', '=',$id)->get();
                    $acs = AllotmentClass::select('id','uacs_accronym')->where('is_deleted', '<>', '1')->orderBy('uacs_accronym', 'ASC')->get();
                    $fcs = FundCluster::select('id','fc_particulars')->where('is_deleted', '<>', '1')->orderBy('fc_particulars', 'ASC')->get();
                    $payees = Payee::select('id','signatory_name')->where('is_deleted', '<>', '1')->orderBy('signatory_name', 'ASC')->get();
                    $rcs = ResponsibilityCenter::select('id','rc_description')->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();
                    $mfos = MfoPap::select('id','mfo_particulars')->where('is_deleted', '<>', '1')->orderBy('mfo_particulars', 'ASC')->get();
                    $uacs = AccountCode::select('id','ac_description')->where('is_deleted', '<>', '1')->orderBy('ac_description', 'ASC')->get();
                    $totalamounts = AccountCode::select('id','budget_total')->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->limit(1)->get();
        return view('budget.edit', compact('ch', 'chdetails', 'acs', 'fcs', 'payees', 'rcs', 'mfos','uacs', 'totalamounts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClaimsHeader  $claimsHeader
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mfocode = ClaimsHeader::find($id);

        return response()->json($mfocode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClaimsHeader  $claimsHeader
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClaimsHeader $claimsHeader)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClaimsHeader  $claimsHeader
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClaimsHeader $claimsHeader)
    {
        //
    }


    public function ac(Request $request)
    {
        $id = $request->input('id');
        $data = Accountcode::select('id','budget_total')
                            ->where('id', '=', $id)
                            ->where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();
        return response()->json($data);
    }

    public function bursPdf($id)
    {

        $ch = DB::table('claims_headers')
                ->join('allotment_classes', 'allotment_classes.id', '=', 'claims_headers.allotment_id')
                ->join('fund_clusters', 'fund_clusters.id', '=', 'claims_headers.fund_cluster_id')
                ->join('payees', 'payees.id', '=', 'claims_headers.payee_id')
                ->select('allotment_classes.uacs_particulars as particulars', 'fund_clusters.fc_code as fundClusters',
                'claims_headers.barcode as barcode', 'payees.signatory_name as payeesName', 'claims_headers.office as office',
                'claims_headers.address as address')
                ->where('claims_headers.id', '=',$id)->first();
        $signA = DB::table('claims_headers')
                ->join('payees', 'payees.id', '=', 'claims_headers.signatory_A_id')
                ->select('payees.signatory_name as payeesName')
                ->where('claims_headers.id', '=',$id)->first();
        $signB = DB::table('claims_headers')
            ->join('payees', 'payees.id', '=', 'claims_headers.signatory_B_id')
            ->select('payees.signatory_name as payeesName')
            ->where('claims_headers.id', '=',$id)->first();
        $chdetails = DB::table('claims_details')
            ->join('responsibility_centers', 'responsibility_centers.id', '=', 'claims_details.rc_id')
            ->join('mfo_paps', 'mfo_paps.id', '=', 'claims_details.mfo_id')
            ->join('account_codes', 'account_codes.id', '=', 'claims_details.uacs_id')
            ->select('responsibility_centers.rc_code as rcCode', 'claims_details.particulars as particulars',
            'mfo_paps.mfo_code as mfoCode', 'account_codes.ac_code as acCode', 'claims_details.amount as amount')
            ->where('claims_details.claim_header_id', '=',$id)->get();
        $total = DB::table('claims_details')->where('claim_header_id', '=', $id)->sum('amount');
        return view('reports.burs', compact('ch','signA','signB', 'chdetails', 'total'));
    }



}
