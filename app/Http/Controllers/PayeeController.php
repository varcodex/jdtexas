<?php

namespace App\Http\Controllers;

use App\Payee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;

class PayeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('payees')
                ->join('responsibility_centers', 'responsibility_centers.id', '=', 'payees.rc_id')
                ->select('payees.id as id', 'payees.signatory_name as sign_name', 'payees.signatory_designation as sign_designation',
                        'payees.address as address', 'payees.email_address as email_address', 'responsibility_centers.rc_description as rc_description',
                        'payees.created_at as created_at', 'payees.updated_at as updated_at')
                ->where('payees.is_deleted', '<>', '1')->orderBy('payees.id', 'ASC')->get();

        $rcs = DB::table('responsibility_centers')
            ->select('id', 'rc_description')
            ->where('is_deleted', '<>', '1')->orderBy('rc_description', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editPayee">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deletePayee">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('payee.index', compact('rcs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'sign_name' => 'required|min:3',
            'sign_designation' => 'required|min:3',
            'address' => 'required|min:3',
            'email_address' => 'required|min:3|email',
            'rc_code' => 'required',
            'is_signatory' => 'required',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(Payee::where([
                                    ['signatory_name', '=', $request->sign_name],
                                    ['signatory_designation', '=', $request->sign_designation],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    Payee::updateOrCreate(['id' => $request->id],
                                                ['signatory_name' => $request->sign_name,
                                                'signatory_designation' => $request->sign_designation,
                                                'address' => $request->address,
                                                'email_address' => $request->email_address,
                                                'rc_id' => $request->rc_code,
                                                'is_signatory' => $request->is_signatory,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(Payee::where([
                                    ['id', '=', Input::get('id')],
                                    ['signatory_name', '=', Input::get('sign_name')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        Payee::find(Input::get('id'))
                                                    ->update(['signatory_name' => $request->sign_name,
                                                            'signatory_designation' => $request->sign_designation,
                                                            'address' => $request->address,
                                                            'email_address' => $request->email_address,
                                                            'rc_id' => $request->rc_code,
                                                            'is_signatory' => $request->is_signatory,
                                                            'is_deleted' => '0']);
                }else{
                    if(Payee::where([
                                        ['id', '=', Input::get('id')],
                                        ['signatory_name', '=', Input::get('sign_name')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        Payee::find(Input::get('id'))
                                        ->update(['signatory_name' => $request->sign_name,
                                                'signatory_designation' => $request->sign_designation,
                                                'address' => $request->address,
                                                'email_address' => $request->email_address,
                                                'rc_id' => $request->rc_code,
                                                'is_signatory' => $request->is_signatory,
                                                'is_deleted' => '0']);
                    }

                }
            }

        return response()->json(['success'=>$request->status]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payee  $payee
     * @return \Illuminate\Http\Response
     */
    public function show(Payee $payee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payee  $payee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('payees')
                ->join('responsibility_centers', 'responsibility_centers.id', '=', 'payees.rc_id')
                ->select('payees.id as id', 'payees.signatory_name as sign_name', 'payees.signatory_designation as sign_designation',
                        'payees.address as address', 'payees.email_address as email_address', 'responsibility_centers.rc_description as rc_description',
                        'responsibility_centers.id as rc_id', 'payees.is_signatory as is_signatory', 'payees.created_at as created_at', 'payees.updated_at as updated_at')
                ->where('payees.id', '=', $id)->where('payees.is_deleted', '<>', '1')->orderBy('payees.id', 'ASC')->get();
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payee  $payee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payee $payee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payee  $payee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payee $payee)
    {
        //
    }

    public function deletePayee($id)
    {
        Payee::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
