<?php

namespace App\Http\Controllers;

use App\Purposes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use DataTables;

class PurposesController extends Controller
{
    public function index()
    {
        $data = Purposes::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();

           // dd($data);
            if (request()->ajax()){
                return DataTables::of($data)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){

                        $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editDT">Edit</a>';
                        $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteDTCode">Delete</a>';
                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
            }

        return view('purpose.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'purpose' => 'required',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(Purposes::where([
                                    ['purpose', '=', $request->purpose],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                    return response()->json(['code'=>'1']);
                }else{
                    Purposes::updateOrCreate(['id' => $request->id],
                                                ['purpose' => $request->purpose,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(Purposes::where([
                                    ['id', '=', Input::get('id')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        Purposes::find(Input::get('id'))
                                                    ->update(['purpose' => $request->purpose,
                                                            'is_deleted' => '0']);
                }else{
                    if(Purposes::where([
                                        ['id', '=', Input::get('id')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        Purposes::find(Input::get('id'))
                                        ->update(['purpose' => $request->purpose,
                                                'is_deleted' => '0']);
                    }
                    
                }
            }

        return response()->json(['success'=>$request->status]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dtcode = Purposes::find($id);
        return response()->json($dtcode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function deleteDTCode($id)
    {
        Purposes::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }

}
