<?php

namespace App\Http\Controllers;
use App\ResponsibilityCenter;
use App\ParentResponsibilityCenter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Session;
use DataTables;

class RCController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('responsibility_centers')
                ->join('parent_responsibility_centers', 'parent_responsibility_centers.id', '=', 'responsibility_centers.prc_id')
                ->select('responsibility_centers.id as id', 'responsibility_centers.rc_code as rc_code', 'responsibility_centers.rc_description as rc_description',
                        'parent_responsibility_centers.prc_code as prc_code', 'responsibility_centers.created_at as created_at', 'responsibility_centers.updated_at as updated_at')
                ->where('responsibility_centers.is_deleted', '<>', '1')->orderBy('responsibility_centers.id', 'ASC')->get();

        $prcs = DB::table('parent_responsibility_centers')
                    ->select('id', 'prc_code')
                    ->where('is_deleted', '<>', '1')->orderBy('prc_code', 'ASC')->get();
                    
        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                    $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editRC">Edit</a>';

                    $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteRC">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('rc.child.index', compact('prcs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'prc_code' => 'required',
            'rc_code' => 'required|min:3',
            'rc_description' => 'required',
        ]);

            if ($validator->fails())
            {
                return response()->json(['errors'=>$validator->errors()->all()]);
            }

            if($request->status == "create"){
                if(ResponsibilityCenter::where([
                                    ['rc_code', '=', $request->rc_code],
                                    ['prc_id', '=', $request->prc_code],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    ResponsibilityCenter::updateOrCreate(['id' => $request->id],
                                                ['prc_id' => $request->prc_code,
                                                'rc_code' => $request->rc_code,
                                                'rc_description' => $request->rc_description,
                                                'is_deleted' => '0']);
                }
            }elseif($request->status == "edit"){
                if(ResponsibilityCenter::where([
                                    ['id', '=', Input::get('id')],
                                    ['rc_code', '=', Input::get('rc_code')],
                                    ['prc_id', '=', Input::get('prc_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        ResponsibilityCenter::find(Input::get('id'))
                                                    ->update(['rc_description' => $request->rc_description,
                                                            'rc_code' => $request->rc_code,
                                                            'prc_id' => $request->prc_code,
                                                            'is_deleted' => '0']);
                }else{
                    if(ResponsibilityCenter::where([
                                        ['id', '=', Input::get('id')],
                                        ['rc_code', '=', Input::get('rc_code')],
                                        ['prc_id', '=', Input::get('prc_code')],
                                        ['is_deleted', '<>', '1']
                                        ])->exists()){
                                            return response()->json(['code'=>'1']);
                    }else{
                        ResponsibilityCenter::find(Input::get('id'))
                                        ->update(['rc_code' => $request->rc_code,
                                                'rc_description' => $request->rc_description,
                                                'prc_id' => $request->prc_code,
                                                'is_deleted' => '0']);
                    }
                    
                }
            }

        return response()->json(['success'=>$request->status]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = DB::table('responsibility_centers')
                ->join('parent_responsibility_centers', 'parent_responsibility_centers.id', '=', 'responsibility_centers.prc_id')
                ->select('responsibility_centers.id as id', 'responsibility_centers.rc_code as rc_code', 'responsibility_centers.rc_description as rc_description',
                'parent_responsibility_centers.id as prc_id', 'parent_responsibility_centers.prc_code as prc_code', 'responsibility_centers.created_at as created_at', 'responsibility_centers.updated_at as updated_at')
                ->where('responsibility_centers.id', '=', $id)->where('responsibility_centers.is_deleted', '<>', '1')->orderBy('responsibility_centers.id', 'ASC')->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    public function deleteRCCode($id)
    {
        ResponsibilityCenter::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }


}
