<?php

namespace App\Http\Controllers;

use App\AllotmentClass;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Session;
use DataTables;

class AllotmentClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AllotmentClass::where('is_deleted', '<>', '1')->orderBy('id', 'ASC')->get();

        if (request()->ajax()){
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){

                       $btn = '<a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="edit btn btn-primary btn-sm editACCode">Edit</a>';

                       $btn = $btn.' <a href="javascript:void(0)" data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-danger btn-sm deleteACCode">Delete</a>';

                        return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('allotments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'ac_code' => 'required|min:1',
            'ac_accronym' => 'required|min:2',
            'ac_particulars' => 'required|min:3',
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        if($request->status == "create"){
            if(AllotmentClass::where([['uacs_code', '=', Input::get('ac_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                                return response()->json(['code'=>'1']);
            }else{
                AllotmentClass::updateOrCreate(['id' => $request->id],
                                ['uacs_code' => $request->ac_code,
                                'uacs_accronym' => $request->ac_accronym,
                                'uacs_particulars' => $request->ac_particulars,
                                'is_deleted' => '0']);
            }
        }elseif($request->status == "edit"){
            if(AllotmentClass::where([
                ['uacs_code', '=', Input::get('ac_code')],
                ['id', '=', Input::get('id')],
                ['is_deleted', '<>', '1']
                ])->exists()){
                    AllotmentClass::find(Input::get('id'))
                                ->update(['uacs_code' => $request->ac_code,
                                        'uacs_accronym' => $request->ac_accronym,
                                        'uacs_particulars' => $request->ac_particulars,
                                        'is_deleted' => '0']);
                }else{
                if(AllotmentClass::where([
                                    ['uacs_code', '=', Input::get('ac_code')],
                                    ['is_deleted', '<>', '1']
                                    ])->exists()){
                                        return response()->json(['code'=>'1']);
                }else{
                    AllotmentClass::find(Input::get('id'))
                                    ->update(['uacs_code' => $request->ac_code,
                                            'uacs_accronym' => $request->ac_accronym,
                                            'uacs_particulars' => $request->ac_particulars,
                                            'is_deleted' => '0']);
                }

                }
        }

        return response()->json(['success'=>'Allotment Class saved successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AllotmentClass  $allotmentClass
     * @return \Illuminate\Http\Response
     */
    public function show(AllotmentClass $allotmentClass)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AllotmentClass  $allotmentClass
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $accode = AllotmentClass::find($id);
        return response()->json($accode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AllotmentClass  $allotmentClass
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AllotmentClass $allotmentClass)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AllotmentClass  $allotmentClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(AllotmentClass $allotmentClass)
    {
        //
    }

    public function deleteAC($id)
    {
        AllotmentClass::updateOrCreate(['id' => $id],
                                    ['is_deleted' => '1']);

        return response()->json(['successfully deleted']);
    }
}
