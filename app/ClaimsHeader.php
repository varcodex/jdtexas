<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimsHeader extends Model
{
    protected $guarded = [];
    public $table = "claims_headers";
    protected $fillable = ['id', 'barcode', 'allotment_id', 'fund_cluster_id', 'year', 'month', 'payee_id',
                            'office', 'address', 'signatory_A_id', 'signatory_B_id',
                            'user_id', 'status', 'is_deleted', 'created_at', 'updated_at'];
}
