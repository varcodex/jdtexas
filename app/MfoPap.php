<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MfoPap extends Model
{
    protected $guarded = [];
    public $table = "mfo_paps";
    protected $fillable = ['id', 'mfo_code', 'mfo_particulars',
                            'created_at', 'updated_at', 'is_deleted'];
}
