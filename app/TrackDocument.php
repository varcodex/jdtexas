<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrackDocument extends Model
{
    protected $guarded = [];
    public $table = "track_documents";
    protected $fillable = ['id', 'tracking_no', 'title','type','purpose_id', 'remarks','office_action','orig_office','current_office','payee_id','user_id','track_status', 'is_deleted', 'created_at', 'updated_at'];
}
