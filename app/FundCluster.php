<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FundCluster extends Model
{
    protected $guarded = [];
    public $table = "fund_clusters";
    protected $fillable = ['id', 'fc_code', 'fc_particulars', 'is_deleted',
                            'created_at', 'updated_at'];
}
